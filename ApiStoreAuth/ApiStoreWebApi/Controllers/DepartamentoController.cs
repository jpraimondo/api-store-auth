﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ApiStore.WebApi.Controllers
{

    [Route("api/departamento")]
    [ApiController]
    public class DepartamentoController : Controller
    {
        private readonly IDepartamentoService _departamentoService;


        /// <summary>
        /// Constructor del controlador recibe IDepartamentoService como dependencia
        /// </summary>
        public DepartamentoController(IDepartamentoService departamentoService)
        {
            _departamentoService = departamentoService;
        }


        /// GET: api/Departamento/
        /// <summary>
        /// Obtiene los departamento registrados.
        /// </summary>
        /// <remarks>
        /// Esta api devuelve una lista con los departamentos registrados.
        /// </remarks>
        /// <response code="200">OK. Devuelve el listado de actividades.</response>        
        /// <response code="400">BadRequest. Un error al buscar los datos solicitados.</response>  
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ViewDepartamentoDto>>>GetAll()
        {
            try
            {
                var departamentList = await _departamentoService.GetAll();

                return Ok(departamentList);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// GET: api/Departamento/{id}
        /// <summary>
        /// Obtiene departamento y las localidades con el id enviado.
        /// </summary>
        /// <remarks>
        /// Esta api busca un departamento por el Id dado.
        /// </remarks>
        /// <param name="id">Id (int) del Departamento.</param>        
        /// <response code="200">OK. Devuelve la actividad con el id enviado.</response> 
        /// <response code="400">BadRequest. Un error al buscar los datos solicitados.</response>   
        /// <response code="404">NotFound. No se encontro la actividad con la id enviada.</response> 
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet("{id}")]
        public async Task<ActionResult<ViewDetailDepartamentoDto>>Get(int id)
        {
            try
            {
                var departamento = await _departamentoService.GetById(id);

                if (departamento.Equals(null))
                    return NotFound();


                return Ok(departamento);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// POST: api/Departamento
        /// <summary>
        /// Almacena un nuevo departamento en la base de datos.
        /// </summary>
        /// <remarks>
        /// Esta api recibe un nuevo departamento enviada en el body, y la inserta en la base de datos.
        /// </remarks>
        /// <param name="createLocalidadDto"> DTO de Actividad.</param>
        /// <response code="400">BadRequest. Un error al insertar los datos solicitados.</response>   
        /// <response code="401">Unauthorized. No se ha indicado, es incorrecto el Token JWT de acceso o no tiene rol de administrador.</response>              
        /// <response code="200">OK. Devuelve la actividad registrada.</response> 
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult<ViewDetailDepartamentoDto>> Post([FromForm] CreateDepartamentoDto createDepartamentoDto)
        {
            try
            {
                var departamento = await _departamentoService.Insert(createDepartamentoDto);
                if (departamento.Id > 0)
                    return Ok(departamento);
                else
                    return BadRequest("Error al insertar no existe el Id, o Id no registrado");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        /// PUT: api/Departamento
        /// <summary>
        /// Actualiza un departamento en la base de datos.
        /// </summary>
        /// <remarks>
        /// Esta api recibe una actualizacion del departaemnto, y la actualiza en la base de datos.
        /// </remarks>
        /// <param name="createLocalidadDto"> DTO de Actividad.</param>
        /// <response code="400">BadRequest. Un error al Actualizar los datos solicitados, no existe el Id.</response>   
        /// <response code="401">Unauthorized. No se ha indicado, es incorrecto el Token JWT de acceso o no tiene rol de administrador.</response>              
        /// <response code="200">OK. Devuelve la actividad registrada.</response> 
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult<ViewDetailDepartamentoDto>> Put([FromForm] CreateDepartamentoDto createDepartamentoDto, int idDepartamento)
        {
            try
            {
                var departamento = await _departamentoService.Update(createDepartamentoDto,idDepartamento);
                if (departamento.Id > 0)
                    return Ok(departamento);
                else
                    return BadRequest("Error al insertar no existe el Id, o Id no registrado");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
    }
}
