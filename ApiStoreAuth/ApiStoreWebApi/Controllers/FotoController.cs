﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Application.Interfaces;
using ApiStore.Application.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ApiStore.WebApi.Controllers
{

    [Route("api/foto")]
    [ApiController]
    public class FotoController : Controller
    {

        private readonly IFotoService _fotoService;

        public FotoController(IFotoService fotoService)
        {
            _fotoService = fotoService;
        }






        /// POST: api/foto
        /// Almacena una nueva foto de una tienda en la base de datos.
        /// </summary>
        /// <remarks>
        /// Esta api recibe una nueva foto de la tienda enviada en el body, y la inserta en la base de datos.
        /// </remarks>
        /// <param name="createFotoDto"> DTO de Foto.</param>
        /// <response code="400">BadRequest. Un error al insertar los datos solicitados.</response>   
        /// <response code="401">Unauthorized. No se ha indicado, es incorrecto el Token JWT de acceso o no tiene rol de administrador.</response>              
        /// <response code="200">OK. Devuelve la categoria registrada.</response> 
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult<ViewFotoDto>> Post([FromForm] CreateFotoDto createFotoDto)
        {
            try
            {
                var foto = await _fotoService.Insert(createFotoDto);
                return Ok(foto);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
    }
}
