﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Application.Interfaces;
using ApiStore.Application.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ApiStore.WebApi.Controllers
{
    [Route("api/tienda")]
    [ApiController]
    public class TiendaController : Controller
    {

        
        private readonly ITiendaService _tiendaService;

        public TiendaController(ITiendaService tiendaService)
        {
            _tiendaService = tiendaService;
        }

        /// GET: api/tienda/
        /// <summary>
        /// Obtiene las tiendas registradas .
        /// </summary>
        /// <remarks>
        /// Esta api devuelve una lista con todas las tiendas registradas.
        /// </remarks>
        /// <response code="200">OK. Devuelve el listado de las tiendas.</response>        
        /// <response code="400">BadRequest. Un error al buscar los datos solicitados.</response>  
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ViewTiendaDto>>> GetAll()
        {
            try
            {
                var tiendaList = await _tiendaService.GetAll();
                return Ok(tiendaList);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// GET: api/tienda/{id}
        /// <summary>
        /// Obtiene la tienda con el id enviado.
        /// </summary>
        /// <remarks>
        /// Esta api busca una tienda por el Id dado.
        /// </remarks>
        /// <param name="id">Id (int) de la tienda.</param>        
        /// <response code="200">OK. Devuelve la tienda con el id enviado.</response> 
        /// <response code="400">BadRequest. Un error al buscar los datos solicitados.</response>   
        /// <response code="404">NotFound. No se encontro la categoria con la id enviada.</response> 
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet("{id}")]
        public async Task<ActionResult<ViewDetailTiendaDto>> Get(int id)
        {
            try
            {
                var tienda = await _tiendaService.GetById(id);
                return Ok(tienda);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// POST: api/tienda
        /// <summary>
        /// Almacena una nueva tienda en la base de datos.
        /// </summary>
        /// <remarks>
        /// Esta api recibe una nueva tienda enviada en el body, y la inserta en la base de datos.
        /// </remarks>
        /// <param name="createTiendaDto"> DTO de Tienda.</param>
        /// <response code="400">BadRequest. Un error al insertar los datos solicitados.</response>   
        /// <response code="401">Unauthorized. No se ha indicado, es incorrecto el Token JWT de acceso o no tiene rol de administrador.</response>              
        /// <response code="200">OK. Devuelve la categoria registrada.</response> 
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult<ViewDetailTiendaDto>> Post([FromForm] CreateTiendaDto createTiendaDto)
        {
            try
            {
                var tienda = await _tiendaService.Insert(createTiendaDto);
                return Ok(tienda);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }


        /// POST: api/tienda/localidad/{id}
        /// <summary>
        /// Lista todas las tiendas de la localidad de la id enviada.
        /// </summary>
        /// <remarks>
        /// Esta api recibe una id int y devuelve una lista DTO con las localidades.
        /// </remarks>
        /// <param name="id"> id de la localidad a buscar.</param>
        /// <response code="400">BadRequest. Un error al insertar los datos solicitados.</response>   
        /// <response code="401">Unauthorized. No se ha indicado, es incorrecto el Token JWT de acceso o no tiene rol de administrador.</response>              
        /// <response code="200">OK. Devuelve la categoria registrada.</response> 
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [HttpGet("localidad/{id}")]
        public async Task<ActionResult<IEnumerable<ViewTiendaDto>>> TiendaPorLocalidad(int id)
        {
            try
            {
                var tienda = await _tiendaService.BuscarPorLocalidad(id);
                return Ok(tienda);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }


        /// GET: api/tienda/slug/{slug}
        /// <summary>
        /// Lista todas las tiendas de la localidad de la id enviada.
        /// </summary>
        /// <remarks>
        /// Esta api recibe una id int y devuelve una lista DTO con las localidades.
        /// </remarks>
        /// <param name="slug"> slug de la localidad a buscar.</param>
        /// <response code="400">BadRequest. Un error al insertar los datos solicitados.</response>         
        /// <response code="200">OK. Devuelve la tienda por el slug dado.</response> 
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [HttpGet("slug/{slug}")]
        public async Task<ActionResult<ViewDetailTiendaDto>> TiendaPorSlug(string slug)
        {
            try
            {
                var tienda = await _tiendaService.Find(t => t.Slug.Equals(slug));
                
                return Ok(tienda.FirstOrDefault());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }




    }
}
