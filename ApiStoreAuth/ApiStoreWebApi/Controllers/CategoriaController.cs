﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ApiStore.WebApi.Controllers
{
    [Route("api/categoria")]
    [ApiController]
    public class CategoriaController : Controller
    {
        private readonly ICategoryService _categoryService;


        public CategoriaController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }


        /// GET: api/Categoria/
        /// <summary>
        /// Obtiene las categorias registradas .
        /// </summary>
        /// <remarks>
        /// Esta api devuelve una lista con las categorias registradas.
        /// </remarks>
        /// <response code="200">OK. Devuelve el listado de las categorias.</response>        
        /// <response code="400">BadRequest. Un error al buscar los datos solicitados.</response>  
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ViewCategoryDto>>> GetAll()
        {
            try
            {
                var categoryList = await _categoryService.GetAll();
                return Ok(categoryList);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// GET: api/Categoria/{id}
        /// <summary>
        /// Obtiene la categoria con el id enviado.
        /// </summary>
        /// <remarks>
        /// Esta api busca una categoria por el Id dado.
        /// </remarks>
        /// <param name="id">Id (int) de la categoria.</param>        
        /// <response code="200">OK. Devuelve la categoria con el id enviado.</response> 
        /// <response code="400">BadRequest. Un error al buscar los datos solicitados.</response>   
        /// <response code="404">NotFound. No se encontro la categoria con la id enviada.</response> 
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet("{id}")]
        public async Task<ActionResult<ViewDetailCategoryDto>> Get(int id)
        {
            try
            {
                var categoria = await _categoryService.GetById(id);
                return Ok(categoria);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// POST: api/Categoria
        /// Almacena una nueva categoria en la base de datos.
        /// </summary>
        /// <remarks>
        /// Esta api recibe una nueva categoria enviada en el body, y la inserta en la base de datos.
        /// </remarks>
        /// <param name="createCategoryDto"> DTO de Categoria.</param>
        /// <response code="400">BadRequest. Un error al insertar los datos solicitados.</response>   
        /// <response code="401">Unauthorized. No se ha indicado, es incorrecto el Token JWT de acceso o no tiene rol de administrador.</response>              
        /// <response code="200">OK. Devuelve la categoria registrada.</response> 
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
       //[Authorize(Roles = "Administrator")]
        public async Task<ActionResult<ViewDetailCategoryDto>> Post([FromForm] CreateCategoryDto createCategoryDto)
        {
            try
            {
                var categoria = await _categoryService.Insert(createCategoryDto);
                return Ok(categoria);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }



    }
}
