﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ApiStore.WebApi.Controllers
{
    [Route("api/localidad")]
    [ApiController]
    public class LocalidadController : Controller
    {
        private readonly ILocalidadService _localidadService;


        public LocalidadController(ILocalidadService localidadService)
        {
            _localidadService = localidadService;
        }


        /// GET: api/Localidad/
        /// <summary>
        /// Obtiene las localidades registradas .
        /// </summary>
        /// <remarks>
        /// Esta api devuelve una lista con las localidades registradas.
        /// </remarks>
        /// <response code="200">OK. Devuelve el listado de las localidades.</response>        
        /// <response code="400">BadRequest. Un error al buscar los datos solicitados.</response>  
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ViewLocalidadDto>>> GetAll()
        {
            try
            {
                var localidadList = await _localidadService.GetAll();
                return Ok(localidadList);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// GET: api/Localidad/{id}
        /// <summary>
        /// Obtiene la localidad id enviado.
        /// </summary>
        /// <remarks>
        /// Esta api busca una localidad por el Id dado.
        /// </remarks>
        /// <param name="id">Id (int) de la localidad.</param>        
        /// <response code="200">OK. Devuelve la localidad con el id enviado.</response> 
        /// <response code="400">BadRequest. Un error al buscar los datos solicitados.</response>   
        /// <response code="404">NotFound. No se encontro la localidad con la id enviada.</response> 
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet("{id}")]
        public async Task<ActionResult<ViewLocalidadDto>> Get(int id)
        {
            try
            {
                var localidad = await _localidadService.GetById(id);
                return Ok(localidad);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// POST: api/Localidad
        /// <summary>
        /// Almacena una nueva localidad en la base de datos.
        /// </summary>
        /// <remarks>
        /// Esta api recibe una nueva localidad enviada en el body, y la inserta en la base de datos.
        /// </remarks>
        /// <param name="createLocalidadDto"> DTO de Actividad.</param>
        /// <response code="400">BadRequest. Un error al insertar los datos solicitados.</response>   
        /// <response code="401">Unauthorized. No se ha indicado, es incorrecto el Token JWT de acceso o no tiene rol de administrador.</response>              
        /// <response code="200">OK. Devuelve la actividad registrada.</response> 
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult<ViewDetailCategoryDto>> Post([FromForm] CreateLocalidadDto createLocalidadDto)
        {
            try
            {
                var localidad = await _localidadService.Insert(createLocalidadDto);
                return Ok(localidad);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }



    }
}
