﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ApiStore.WebApi.Controllers
{
    [Route("api/redes")]
    [ApiController]
    public class RedesController : Controller
    {
        private readonly IRedesService _redesServices;

        public RedesController(IRedesService redesService)
        {
            _redesServices = redesService;
        }

        /// GET: api/redes/
        /// <summary>
        /// Obtiene las redes registrados.
        /// </summary>
        /// <remarks>
        /// Esta api devuelve una lista con las redes registrados.
        /// </remarks>
        /// <response code="200">OK. Devuelve el listado de redes.</response>        
        /// <response code="400">BadRequest. Un error al buscar los datos solicitados.</response>  
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ViewRedesDto>>> GetAll()
        {
            try
            {
                var listTodasRedes = await _redesServices.GetAll();

                return Ok(listTodasRedes);

            }
            catch (Exception ex)
            {

                return BadRequest($"Error al buscar los datos: {ex.Message}");
            }
        }

        /// GET: api/redes/store/id
        /// <summary>
        /// Obtiene las redes registradas de una tienda.
        /// </summary>
        /// <remarks>
        /// Esta api devuelve una lista con las redes registradas de una tienda.
        /// </remarks>
        /// <param name="id">Id (int) de la tienda.</param>  
        /// <response code="200">OK. Devuelve el listado de redes.</response>        
        /// <response code="400">BadRequest. Un error al buscar los datos solicitados.</response>  
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet("store/{id}")]
        public async Task<ActionResult<IEnumerable<ViewRedesDto>>> GetByStore(int id)
        {
            try
            {
                var listTodasRedes = await _redesServices.GetByStore(id);

                return Ok(listTodasRedes);

            }
            catch (Exception ex)
            {

                return BadRequest($"Error al buscar los datos: {ex.Message}");
            }
        }


        /// POST: api/redes
        /// <summary>
        /// Almacena una nueva red a la empresa en la base de datos.
        /// </summary>
        /// <remarks>
        /// Esta api recibe un nueva redes enviada en el body, y la inserta en la base de datos.
        /// </remarks>
        /// <param name="createRedesDto"> DTO de redes.</param>
        /// <response code="400">BadRequest. Un error al insertar los datos solicitados.</response>        
        /// <response code="200">OK. Devuelve la red registrada.</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public async Task<ActionResult<ViewRedesDto>> Post([FromForm] CreateRedesDto createRedesDto)
        {
            try
            {
                var red = await _redesServices.Insert(createRedesDto);
                return Ok(red);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }



    }
}
