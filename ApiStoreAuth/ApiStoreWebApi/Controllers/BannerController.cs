﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Application.Interfaces;
using ApiStore.Application.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ApiStore.WebApi.Controllers
{

    [Route("api/banner")]
    [ApiController]
    public class BannerController : Controller
    {

        private readonly IBannerService _bannerService;


        public BannerController(IBannerService bannerService)
        {
            _bannerService = bannerService;
        }


        /// GET: api/banner/
        /// <summary>
        /// Obtiene llos banner registrados ordenados por el orden.
        /// </summary>
        /// <remarks>
        /// Esta api devuelve una lista con los banner cargados.
        /// </remarks>
        /// <response code="200">OK. Devuelve el listado de los banners.</response>        
        /// <response code="400">BadRequest. Un error al buscar los datos solicitados.</response>  
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ViewCategoryDto>>> GetAll()
        {
            try
            {
                var bannerList = await _bannerService.GetAll();
                return Ok(bannerList.OrderBy(b => b.Orden));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        /// POST: api/banner
        /// Almacena un nuevo banner en la base de datos
        /// </summary>
        /// <remarks>
        /// Esta api recibe un nuevo banner enviada en el body, y la inserta en la base de datos.
        /// </remarks>
        /// <param name="createBannerDto"> DTO de Banner.</param>
        /// <response code="400">BadRequest. Un error al insertar los datos solicitados.</response>   
        /// <response code="401">Unauthorized. No se ha indicado, es incorrecto el Token JWT de acceso o no tiene rol de administrador.</response>              
        /// <response code="200">OK. Devuelve el banner registrado.</response> 
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult<ViewDetailBannerDto>> Post([FromForm] CreateBannerDto createBannerDto)
        {
            try
            {
                var banner = await _bannerService.Insert(createBannerDto);
                return Ok(banner);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

    }
}
