﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ApiStore.WebApi.Controllers
{

    [Route("api/auth")]
    [ApiController]
    public class AuthController : Controller
    {

        private readonly IAuthService authService;

        public AuthController(IAuthService authService)
        {
            this.authService = authService;
        }

        /// POST: api/login
        /// Inicia la nueva sesion en el sistema
        /// </summary>
        /// <remarks>
        /// Esta api recibe un correo y password y devuelve el token al usuario.
        /// </remarks>
        /// <param name="request"> DTO de login.</param>
        /// <response code="400">BadRequest. Un error al insertar los datos solicitados.</response>
        /// <response code="200">OK. Devuelve el token al usuario.</response> 
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost("login")]
        public async Task<ActionResult<ViewAuthDto>> Login([FromBody]CreateAuthDto request)
        {
            try
            {
                return Ok(await authService.Login(request));
            }
            catch (Exception ex)
            {

                return BadRequest($"Error en el usuario o la contraseña: {ex.Message}");
            }
           
        }

        /// POST: api/register
        /// Registra un nuevo usuario en el sistema
        /// </summary>
        /// <remarks>
        /// Esta api recibe datos del usuario.
        /// </remarks>
        /// <param name="request"> DTO de login.</param>
        /// <response code="400">BadRequest. Un error al insertar los datos solicitados.</response>
        /// <response code="200">OK. Devuelve el token al usuario.</response> 
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost("register")]
        public async Task<ActionResult<ViewAuthDto>> Register([FromBody] RegisterAuthDto request)
        {
            try
            {
                return Ok(await authService.Register(request));
            }
            catch (Exception ex)
            {

                return BadRequest($"Error al registrar el usuario: {ex.Message}");
            }  
        }

        /// POST: api/resetpassword
        /// Reset de password del usuario enviado
        /// </summary>
        /// <remarks>
        /// Esta api recibe datos del usuario.
        /// </remarks>
        /// <param name="request"> DTO de login.</param>
        /// <response code="400">BadRequest. Un error al insertar los datos solicitados.</response>
        /// <response code="200">OK. Devuelve el token al usuario.</response> 
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = "Administrator")]
        [HttpPost("resetpassword")]
        public async Task<ActionResult<ViewAuthDto>> ResetPassword([FromBody] RegisterAuthDto request)
        {
            try
            {
                return Ok(await authService.ChangePassword(request));
            }
            catch (Exception ex)
            {

                return BadRequest($"Error en el usuario o la contraseña: {ex.Message}");
            }

        }
    }
}
