﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ApiStore.WebApi.Controllers
{

    [Route("api/newsletter")]
    [ApiController]
    public class NewsletterController : Controller
    {
        
        private readonly INewsletterService _newsletterService;

        public NewsletterController(INewsletterService newsletterService)
        {
            _newsletterService = newsletterService;
        }


        /// GET: api/newslatter/
        /// <summary>
        /// Obtiene las newsletter registradas .
        /// </summary>
        /// <remarks>
        /// Esta api devuelve una lista con las newsletter registradas.
        /// </remarks>
        /// <response code="200">OK. Devuelve el listado de las newsletters.</response>        
        /// <response code="400">BadRequest. Un error al buscar los datos solicitados.</response>
        /// <response code="401">Unauthorized. Usuario no Authorizado.</response>  
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ViewNewsletterDto>>> GetAll()
        {
            try
            {
                var newsletterList = await _newsletterService.GetAll();
                return Ok(newsletterList);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// GET: api/newsletter/{id}
        /// <summary>
        /// Obtiene la localidad id enviado.
        /// </summary>
        /// <remarks>
        /// Esta api busca una newsletter por el Id dado.
        /// </remarks>
        /// <param name="id">Id (int) de la newsletter.</param>        
        /// <response code="200">OK. Devuelve la newsletter con el id enviado.</response> 
        /// <response code="400">BadRequest. Un error al buscar los datos solicitados.</response>   
        /// <response code="404">NotFound. No se encontro la newsletter con la id enviada.</response>
        /// <response code="401">Unauthorized. Usuario no Authorizado.</response>  
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [Authorize]
        [HttpGet("{id}")]
        public async Task<ActionResult<ViewNewsletterDto>> Get(int id)
        {
            try
            {
                var newsletter = await _newsletterService.GetById(id);
                return Ok(newsletter);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// POST: api/newsletter
        /// <summary>
        /// Almacena una nueva newsletter en la base de datos.
        /// </summary>
        /// <remarks>
        /// Esta api recibe un nueva newsletter enviada en el body, y la inserta en la base de datos.
        /// </remarks>
        /// <param name="createNewsletterDto"> DTO de newsletter.</param>
        /// <response code="400">BadRequest. Un error al insertar los datos solicitados o al enviar el correo.</response>        
        /// <response code="403">Forbidden. Un error al enviar el correo.</response>
        /// <response code="200">OK. Devuelve la actividad registrada.</response> 
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost]
        public async Task<ActionResult<ViewNewsletterDto>> Post([FromForm] CreateNewsletterDto createNewsletterDto)
        {
            try
            {
                var response = await _newsletterService.Insert(createNewsletterDto);
                if (response.Id > 0)
                    return Ok(response);
                else
                    return Forbid();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }


    }
}
