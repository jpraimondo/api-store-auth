﻿using Microsoft.AspNetCore.Mvc;

namespace ApiStore.WebApi.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
