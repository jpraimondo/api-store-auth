﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Application.Helpers;
using ApiStore.Application.Interfaces;
using ApiStore.Application.Mapper;
using ApiStore.Domain.Entities;
using ApiStore.Persistence.Repositories.Interfaces;
using System.Linq.Expressions;

namespace ApiStore.Application.Service
{
    public class NewsletterService : INewsletterService
    {
        private readonly IEmailHelper emailHelper;

        public IUnitOfWork unitOfWork;

        public NewsletterService(IUnitOfWork unitOfWork, IEmailHelper emailHelper)
        {
            this.unitOfWork = unitOfWork;
            this.emailHelper = emailHelper;
        }


        public async Task<bool> Delete(int id)
        {
            var newsletter = await unitOfWork.NewsletterRepository.GetById(id);

            if (newsletter == null)
                return false;


            await unitOfWork.NewsletterRepository.Delete(newsletter);
            await unitOfWork.Save();

            return true;
        }

        public IEnumerable<ViewNewsletterDto> Find(Expression<Func<Newsletter, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ViewNewsletterDto>> GetAll()
        {
            var newsletters = await unitOfWork.NewsletterRepository.GetAll();

            var newslettersDto = new List<ViewNewsletterDto>();

            foreach (Newsletter newsletter in newsletters)
            {
                newslettersDto.Add(NewsletterMapper.ToViewNewsletterDto(newsletter));
            }

            return newslettersDto;
        }

        public async Task<IEnumerable<ViewNewsletterDto>> GetAllAndDelete()
        {
            var newsletters = await unitOfWork.NewsletterRepository.GetAll();

            var newslettersDto = new List<ViewNewsletterDto>();

            foreach (Newsletter newsletter in newsletters)
            {
                newslettersDto.Add(NewsletterMapper.ToViewNewsletterDto(newsletter));
            }

            return newslettersDto;
        }

        public async Task<Newsletter> GetById(int id)
        {
            var newsletter = await unitOfWork.NewsletterRepository.GetById(id);

            return newsletter;
        }

        public async Task<ViewNewsletterDto> Insert(CreateNewsletterDto newsletterDto)
        {
            var newsletter = NewsletterMapper.ToNewsletter(newsletterDto);
            newsletter.IsActve = true;
            await unitOfWork.NewsletterRepository.Insert(newsletter);
            
            if(await emailHelper.SendEmail(newsletter.Correo))
            {
                await unitOfWork.Save();
                return NewsletterMapper.ToViewNewsletterDto(newsletter);
            }

            return new ViewNewsletterDto();

        }

        public async Task<ViewNewsletterDto> Update(CreateNewsletterDto newsletterDto, int id)
        {
            var newsletter = await unitOfWork.NewsletterRepository.GetById(id);

            newsletter.Correo = newsletterDto.Correo;

            await unitOfWork.NewsletterRepository.Update(newsletter);

            await unitOfWork.Save();

            return NewsletterMapper.ToViewNewsletterDto(newsletter);
        }
    }
}
