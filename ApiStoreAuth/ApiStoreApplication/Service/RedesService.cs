﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Application.Interfaces;
using ApiStore.Application.Mapper;
using ApiStore.Domain.Entities;
using ApiStore.Persistence.Repositories.Interfaces;
using System.Linq.Expressions;

namespace ApiStore.Application.Service
{
    public class RedesService : IRedesService
    {
        public IUnitOfWork unitOfWork;

        public RedesService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Delete(int id)
        {
            var red = await unitOfWork.RedesRepository.GetById(id);

            if (red == null)
                return false;

            await unitOfWork.RedesRepository.Delete(red);
            _ = unitOfWork.Save();

            return true;
        }

        public IEnumerable<Redes> Find(Expression<Func<Redes, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ViewRedesDto>> GetAll()
        {
            var redes = await unitOfWork.RedesRepository.GetAll();

            var redesDto = new List<ViewRedesDto>();

            foreach (Redes red in redes)
            {
                redesDto.Add(RedesMapper.ToViewRedesDto(red));
            }

            return redesDto;
        }

        public async Task<IEnumerable<ViewRedesDto>> GetAllAndDelete()
        {
            var redes = await unitOfWork.RedesRepository.GetAllAndDelete();

            var redesDto = new List<ViewRedesDto>();

            foreach (Redes red in redes)
            {
                redesDto.Add(RedesMapper.ToViewRedesDto(red));
            }

            return redesDto;
        }

        public async Task<Redes> GetById(int id)
        {
            var red = await unitOfWork.RedesRepository.GetById(id);

            return red;
        }

        public async Task<IEnumerable<ViewRedesDto>> GetByStore(int id)
        {
            var redes = (await unitOfWork.RedesRepository.GetAll()).Where(s => s.TiendaId.Equals(id)).ToList();

            var redesDto = new List<ViewRedesDto>();

            foreach (Redes red in redes)
            {
                redesDto.Add(RedesMapper.ToViewRedesDto(red));
            }

            return redesDto;
        }

        public async Task<ViewDetailRedesDto> Insert(CreateRedesDto redDto)
        {
            var tienda = await unitOfWork.TiendaRepository.GetById(redDto.TiendaId);

            if (tienda is null)
                return new ViewDetailRedesDto();


            var red = RedesMapper.ToRedes(redDto);
            red.Tienda= tienda;

            await unitOfWork.RedesRepository.Insert(red);

            await unitOfWork.Save();

            return RedesMapper.ToViewDetailRedesDto(red);
        }

        public async Task<ViewDetailRedesDto> Update(CreateRedesDto redDto, int id)
        {
            var red = await unitOfWork.RedesRepository.GetById(id);

            red.Link = redDto.Link;
            red.Nombre = redDto.Nombre;
            red.TiendaId = redDto.TiendaId;


            await unitOfWork.RedesRepository.Update(red);

            _ = unitOfWork.Save();

            return RedesMapper.ToViewDetailRedesDto(red);
        }
    }
}
