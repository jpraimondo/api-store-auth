﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Application.Interfaces;
using ApiStore.Application.Mapper;
using ApiStore.Domain.Entities;
using ApiStore.Persistence.Repositories.Interfaces;
using System;
using System.Linq.Expressions;

namespace ApiStore.Application.Service
{
    public class TiendaService : ITiendaService
    {


        public IUnitOfWork unitOfWork;

        public TiendaService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }


        public async Task<bool> Delete(int id)
        {
            var tienda = await unitOfWork.TiendaRepository.GetById(id);

            if (tienda is null)
                return false;

            await unitOfWork.TiendaRepository.Delete(tienda);
            await unitOfWork.Save();

            return true;
        }

        public async Task<IEnumerable<ViewTiendaDto>> Find(Expression<Func<Tienda, bool>> predicate)
        {
            var tiendas = await unitOfWork.TiendaRepository.Find(predicate);

            var tiendasDto = new List<ViewTiendaDto>();

            foreach (Tienda tienda in tiendas)
            {
                tiendasDto.Add(TiendaMapper.ToViewTiendaDto(tienda));
            }
            return tiendasDto;
        }

        public async Task<IEnumerable<ViewTiendaDto>> Find(Expression<Func<Tienda, bool>> predicate,
                                                           Func<IQueryable<Tienda>, IOrderedQueryable<Tienda>> orderBy = null,
                                                           string includeString = null,
                                                           bool disableTracking = true)
        {
            var tiendas = await unitOfWork.TiendaRepository.Find(predicate, orderBy, includeString, disableTracking);

            var tiendasDto = new List<ViewTiendaDto>();

            foreach (Tienda tienda in tiendas)
            {
                tiendasDto.Add(TiendaMapper.ToViewTiendaDto(tienda));
            }
            return tiendasDto;
        }

        public async Task<IEnumerable<ViewTiendaDto>> GetAll()
        {
            var tiendas = await unitOfWork.TiendaRepository.GetAll();

            var tiendasDto = new List<ViewTiendaDto>();

            foreach (Tienda tienda in tiendas)
            {
                tiendasDto.Add(TiendaMapper.ToViewTiendaDto(tienda));
            }

            return tiendasDto;
        }

        
        
        public async Task<IEnumerable<ViewTiendaDto>> BuscarPorLocalidad(int id)
        {
            var tiendas = await unitOfWork.TiendaRepository.Find(t => t.LocalidadId.Equals(id),
                                                                null,
                                                                "Localidad"
                                                                );

            

            var tiendasDto = new List<ViewTiendaDto>();

            foreach (Tienda tienda in tiendas)
            {
                tiendasDto.Add(TiendaMapper.ToViewTiendaDto(tienda));
            }


            return tiendasDto;
        }


        public async Task<IEnumerable<ViewTiendaDto>> GetAllAndDelete()
        {
            var tiendas = await unitOfWork.TiendaRepository.GetAll();

            var tiendasDto = new List<ViewTiendaDto>();

            foreach (Tienda tienda in tiendas)
            {
                tiendasDto.Add(TiendaMapper.ToViewTiendaDto(tienda));
            }

            return tiendasDto;
        }

        public async Task<ViewDetailTiendaDto> GetById(int id)
        {
            var tienda = (await unitOfWork.TiendaRepository.Find(t => t.Id.Equals(id),
                                                                null,
                                                                "Fotos",
                                                                false)).FirstOrDefault();

            if(tienda is not null)
            {
                return Mapper.TiendaMapper.ToViewDetailDto(tienda);
            }


            return new ViewDetailTiendaDto();
        }

        public async Task<ViewDetailTiendaDto> Insert(CreateTiendaDto tiendaDto)
        {
            var tienda = TiendaMapper.ToTienda(tiendaDto);

            tienda.Slug = await CreateSlug(tiendaDto.Nombre);

            tienda.Localidad = await unitOfWork.LocalidadRepository.GetById(tiendaDto.LocalidadId);

            tienda.Category = await unitOfWork.CategoryRepository.GetById(tiendaDto.CategoriaId);

            await unitOfWork.TiendaRepository.Insert(tienda);

            await unitOfWork.Save();

            return TiendaMapper.ToViewDetailDto(tienda);
        }

        public async Task<ViewDetailTiendaDto> Update(CreateTiendaDto tiendaDto, int id)
        {
            var tienda = await unitOfWork.TiendaRepository.GetById(id);

            tienda.Nombre = tiendaDto.Nombre;
            tienda.Descripcion = tiendaDto.Descripcion;
            tienda.Telefono = tiendaDto.Telefono;
            tienda.Slug = await CreateSlug(tiendaDto.Nombre);

            if (tienda.LocalidadId != tiendaDto.LocalidadId)
            {
                tienda.Localidad = await unitOfWork.LocalidadRepository.GetById(tiendaDto.LocalidadId);
                tienda.LocalidadId = tiendaDto.LocalidadId;
            }


            await unitOfWork.TiendaRepository.Update(tienda);

            _ = unitOfWork.Save();

            return TiendaMapper.ToViewDetailDto(tienda) ;
        }

        private async Task<string> CreateSlug(string nombre)
        {

            string slug = nombre.Replace(" ", "-").ToLower();

            var tiendas = await unitOfWork.TiendaRepository.GetAllAndDelete();

            var tienda = tiendas.Where(t => t.Slug.Equals(slug)).FirstOrDefault();

            if (tienda == null)
            {
                return slug;
            }
            else
            {
                var random = new Random();
                var num = random.Next(0, 10000);

                slug = $"{slug}-{num}";

                return slug;
            }
        }
    }
}
