﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Application.Interfaces;
using ApiStore.Application.Mapper;
using ApiStore.Domain.Entities;
using ApiStore.Persistence.Repositories.Interfaces;
using System.Linq.Expressions;

namespace ApiStore.Application.Service
{
    public class DepartamentoService : IDepartamentoService
    {
        public IUnitOfWork unitOfWork;


        public DepartamentoService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Delete(int id)
        {
            var departamento = await unitOfWork.DepartamentoRepository.GetById(id);

            if (departamento == null)
                return false;


            await unitOfWork.DepartamentoRepository.Delete(departamento);
            await unitOfWork.Save();
            return true;
        }

        public IEnumerable<Departamento> Find(Expression<Func<Departamento, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ViewDepartamentoDto>> GetAll()
        {
            var departamentos = await unitOfWork.DepartamentoRepository.GetAll();

            var departamentosDto = new List<ViewDepartamentoDto>();

            foreach (Departamento departamento in departamentos)
            {
                departamentosDto.Add(DepartamentoMapper.ToViewDepartamentoDto(departamento));
            }

            return departamentosDto;
        }

        public async Task<IEnumerable<ViewDepartamentoDto>> GetAllAndDelete()
        {
            var departamentos = await unitOfWork.DepartamentoRepository.GetAllAndDelete();

            var departamentosDto = new List<ViewDepartamentoDto>();

            foreach (Departamento departamento in departamentos)
            {
                departamentosDto.Add(DepartamentoMapper.ToViewDepartamentoDto(departamento));
            }

            return departamentosDto;
        }

        public async Task<ViewDetailDepartamentoDto> GetById(int id)
        {
            var departamento = DepartamentoMapper.ToViewDetailDepartamentoDto(await unitOfWork.DepartamentoRepository.GetById(id));

            var localidades = (await unitOfWork.LocalidadRepository.Find(l => l.DepartamentoId == id)).ToList();

            departamento.Localidades = LocalidadMapper.ToViewDetailLocalidadDto(localidades);

            return departamento;
        }

        public async Task<ViewDetailDepartamentoDto> Insert(CreateDepartamentoDto departamentoDto)
        {
            var departamento = DepartamentoMapper.ToDepartamento(departamentoDto);

            await unitOfWork.DepartamentoRepository.Insert(departamento);

            await unitOfWork.Save();

            return DepartamentoMapper.ToViewDetailDepartamentoDto(departamento);


        }

        public async Task<ViewDetailDepartamentoDto> Update(CreateDepartamentoDto departamentoDto, int id)
        {
            var departamento = await unitOfWork.DepartamentoRepository.GetById(id);

            if(departamento is null)
                return new ViewDetailDepartamentoDto();


            departamento.Nombre = departamentoDto.Nombre;

            await unitOfWork.DepartamentoRepository.Update(departamento);

            await unitOfWork.Save();

            return DepartamentoMapper.ToViewDetailDepartamentoDto(departamento);
        }
    }
}
