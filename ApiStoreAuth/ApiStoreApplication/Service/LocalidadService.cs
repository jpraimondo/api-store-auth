﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Application.Interfaces;
using ApiStore.Application.Mapper;
using ApiStore.Domain.Entities;
using ApiStore.Persistence.Repositories.Interfaces;
using System.Linq.Expressions;

namespace ApiStore.Application.Service
{
    public class LocalidadService : ILocalidadService
    {

        public IUnitOfWork unitOfWork;

        public LocalidadService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }



        public async Task<bool> Delete(int id)
        {
            var localidad = await unitOfWork.LocalidadRepository.GetById(id);

            if (localidad == null)
                return false;


            await unitOfWork.LocalidadRepository.Delete(localidad);
            await unitOfWork.Save();

            return true;
        }

        public IEnumerable<Localidad> Find(Expression<Func<Localidad, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ViewLocalidadDto>> GetAll()
        {
            var localidades = await unitOfWork.LocalidadRepository.GetAll();

            var localidadesDto = new List<ViewLocalidadDto>();

            foreach (Localidad localidad in localidades)
            {
                localidadesDto.Add(LocalidadMapper.ToViewLocalidadDto(localidad));
            }

            return localidadesDto;
        }

        public async Task<IEnumerable<ViewLocalidadDto>> GetAllAndDelete()
        {
            var localidades = await unitOfWork.LocalidadRepository.GetAll();

            var localidadesDto = new List<ViewLocalidadDto>();

            foreach (Localidad localidad in localidades)
            {
                localidadesDto.Add(LocalidadMapper.ToViewLocalidadDto(localidad));
            }

            return localidadesDto;
        }

        public async Task<Localidad> GetById(int id)
        {
            var localidad = await unitOfWork.LocalidadRepository.GetById(id);

            return localidad;
        }

        public async Task<ViewDetailLocalidadDto> Insert(CreateLocalidadDto localidadDto)
        {
            var localidad = LocalidadMapper.ToLocalidad(localidadDto);

            localidad.Departamento = await unitOfWork.DepartamentoRepository.GetById(localidadDto.DepartamentoId);

            await unitOfWork.LocalidadRepository.Insert(localidad);

            await unitOfWork.Save();

            return LocalidadMapper.ToViewDetailLocalidadDto(localidad);
        }

        public async Task<Localidad> Update(CreateLocalidadDto localidadDto, int id)
        {
            var localidad = await unitOfWork.LocalidadRepository.GetById(id);

            localidad.Nombre = localidadDto.Nombre;

            localidad.Departamento = await unitOfWork.DepartamentoRepository.GetById(localidadDto.DepartamentoId);


            await unitOfWork.LocalidadRepository.Update(localidad);

            await unitOfWork.Save();

            return localidad;
        }
    }
}
