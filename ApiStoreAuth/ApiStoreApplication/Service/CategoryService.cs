﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Application.Interfaces;
using ApiStore.Application.Mapper;
using ApiStore.Domain.Constants;
using ApiStore.Domain.Entities;
using ApiStore.Persistence.Repositories.Interfaces;
using System.Linq.Expressions;

namespace ApiStore.Application.Service
{
    public class CategoryService : ICategoryService
    {
        private IUnitOfWork unitOfWork;
        private IFileHelper? fileHelper;
        
        public CategoryService(IUnitOfWork unitOfWork, IFileHelper fileHelper)
        {
            this.unitOfWork = unitOfWork;
            this.fileHelper = fileHelper;
        }


        public async Task<bool> Delete(int id)
        {
            var category = await unitOfWork.CategoryRepository.GetById(id);

            if (category == null)
                return false;


            await unitOfWork.CategoryRepository.Delete(category);
            await unitOfWork.Save();
            return true;

        }

        public IEnumerable<Category> Find(Expression<Func<Category, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ViewCategoryDto>> GetAll()
        {

            var categories = await unitOfWork.CategoryRepository.GetAll();

            var categoriesDto = new List<ViewCategoryDto>();

            foreach (Category category in categories)
            {
                categoriesDto.Add(CategoryMapper.ToViewCategoryDto(category));
            }

            return categoriesDto;
        }

        public async Task<IEnumerable<ViewCategoryDto>> GetAllAndDelete()
        {
            var categories = await unitOfWork.CategoryRepository.GetAllAndDelete();

            var categoriesDto = new List<ViewCategoryDto>();

            foreach (Category category in categories)
            {
                categoriesDto.Add(CategoryMapper.ToViewCategoryDto(category));
            }

            return categoriesDto;
        }

        public async Task<ViewDetailCategoryDto> GetById(int id)
        {
            var categories = await unitOfWork.CategoryRepository.Find(c => c.Id.Equals(id),null,"Tiendas",true);

            var category = categories.FirstOrDefault();

            return CategoryMapper.ToViewDetailCategoryDto(category);
        }

        public async Task<ViewDetailCategoryDto> Insert(CreateCategoryDto categoryDto)
        {
            var category = CategoryMapper.ToCategory(categoryDto);

            if (categoryDto.UrlImagen is not null)
            {
                category.UrlImagen = await fileHelper.SaveImage(categoryDto.UrlImagen, ConstantsApp.Contenedores.ConetenedorDeImagenesCategoria);
            }

            await unitOfWork.CategoryRepository.Insert(category);

            await unitOfWork.Save();

            return CategoryMapper.ToViewDetailCategoryDto(category);

        }

        public async Task<Category> Update(CreateCategoryDto categoryDto, int id)
        {
            var category = await unitOfWork.CategoryRepository.GetById(id);

            category.Nombre = categoryDto.Nombre;

            if (categoryDto.UrlImagen != null)
            {
                category.UrlImagen = await fileHelper.SaveImage(categoryDto.UrlImagen, ConstantsApp.Contenedores.ConetenedorDeImagenesCategoria);
            };

            await unitOfWork.CategoryRepository.Update(category);

            await unitOfWork.Save();

            return category;
        }

        public async Task<bool> Activate(int id)
        {
            try
            {
                var category = await unitOfWork.CategoryRepository.GetById(id);
                await unitOfWork.CategoryRepository.Activate(category);
                await unitOfWork.Save();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> Inactivate(int id)
        {
            try
            {
                var category = await unitOfWork.CategoryRepository.GetById(id);
                await unitOfWork.CategoryRepository.Inactivate(category);
                await unitOfWork.Save();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


    }
}
