﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.Identity.Interfaces;
using ApiStore.Application.Identity.Models;
using ApiStore.Application.Interfaces;
using ApiStore.Application.Mapper;

namespace ApiStore.Application.Service
{

    public class AuthService : IAuthService
    {

        private readonly IIdentityService _identityService;
        private readonly AuthMapper _authMapper;

        public AuthService(IIdentityService identityService)
        {
            _identityService = identityService;
            _authMapper = new AuthMapper();
        }

        public async Task<ViewAuthDto> Login(CreateAuthDto request)
        {
            try
            {
                AuthResponse authResponse = await _identityService.Login(AuthMapper.ToAuthRequest(request));

                return new ViewAuthDto
                {
                    AuthResponse = authResponse,
                    Message = "Usuario logueado con exito."

                };
                
            }
            catch (Exception ex)
            {

                return new ViewAuthDto
                {
                    AuthResponse = null,
                    Message = ex.Message

                };
            } 
        }

        public async Task<ViewAuthDto> Register(RegisterAuthDto request)
        {
            try
            {
                AuthResponse authResponse = await _identityService.Register(AuthMapper.ToRegisterRequest(request));

                return new ViewAuthDto
                {
                    AuthResponse = authResponse,
                    Message = "Usuario registrado con exito."

                };
            }
            catch (Exception ex)
            {

                return new ViewAuthDto
                {
                    AuthResponse = null,
                    Message = ex.Message

                };
            }
        }

        public async Task<ViewAuthDto> ChangePassword(RegisterAuthDto request)
        {
            try
            {
                AuthResponse authResponse = await _identityService.SetPassword(AuthMapper.ToRegisterRequest(request));

                return new ViewAuthDto
                {
                    AuthResponse = authResponse,
                    Message = "Se cambio el password con exito."

                };
            }
            catch (Exception ex)
            {

                return new ViewAuthDto
                {
                    AuthResponse = null,
                    Message = ex.Message

                };
            }
        }
    }
}
