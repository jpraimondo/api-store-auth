﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Application.Interfaces;
using ApiStore.Application.Mapper;
using ApiStore.Domain.Constants;
using ApiStore.Domain.Entities;
using ApiStore.Persistence.Repositories.Interfaces;
using System.Linq.Expressions;

namespace ApiStore.Application.Service
{
    internal class BannerService : IBannerService
    {

        private readonly IUnitOfWork unitOfWork;
        private readonly IFileHelper? fileHelper;

        public BannerService(IUnitOfWork unitOfWork, IFileHelper fileHelper)
        {
            this.unitOfWork = unitOfWork;
            this.fileHelper = fileHelper;
        }



        public Task<bool> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ViewDetailBannerDto> Find(Expression<Func<Banner, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ViewDetailBannerDto>> GetAll()
        {
            var banners = await unitOfWork.BannerRepository.GetAll();

            var bannersDto = new List<ViewDetailBannerDto>();

            foreach (Banner banner in banners)
            {
                bannersDto.Add(BannerMapper.ToViewDetailBannerDto(banner));
            }

            return bannersDto;
        }

        public Task<IEnumerable<ViewDetailBannerDto>> GetAllAndDelete()
        {
            throw new NotImplementedException();
        }

        public Task<ViewDetailBannerDto> GetById(int? id)
        {
            throw new NotImplementedException();
        }

        public async Task<ViewDetailBannerDto> Insert(CreateBannerDto createBannerDto)
        {
            var banner = BannerMapper.ToBanner(createBannerDto);

            if (createBannerDto.BannerImage is not null)
            {
                banner.UrlBanner = await fileHelper.SaveImage(createBannerDto.BannerImage, ConstantsApp.Contenedores.ConetenedorDeImagenes);
            }

            await unitOfWork.BannerRepository.Insert(banner);

            await unitOfWork.Save();

            return BannerMapper.ToViewDetailBannerDto(banner);
        }

        public Task<ViewDetailBannerDto> Update(CreateBannerDto createBannerDto, int id)
        {
            throw new NotImplementedException();
        }
    }
}
