﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.Interfaces;
using ApiStore.Domain.Constants;
using ApiStore.Domain.Entities;
using ApiStore.Persistence.Repositories.Interfaces;
using System.Linq.Expressions;

namespace ApiStore.Application.Service
{
    public class FotoService : IFotoService
    {

        private IUnitOfWork _unitOfWork;
        private IFileHelper? _fileHelper;



        public FotoService(IUnitOfWork unitOfWork, IFileHelper? fileHelper)
        {
            _unitOfWork = unitOfWork;
            _fileHelper = fileHelper;
        }

        public Task<bool> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ViewFotoDto> Find(Expression<Func<Foto, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<ViewFotoDto>> GetAll()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<ViewFotoDto>> GetAllAndDelete()
        {
            throw new NotImplementedException();
        }

        public Task<ViewFotoDto> GetById(int? id)
        {
            throw new NotImplementedException();
        }

        public async Task<ViewFotoDto> Insert(CreateFotoDto createFotoDto)
        {

            Foto foto = Mapper.FotoMapper.ToFoto(createFotoDto);

            
            if(!(createFotoDto.UrlFoto is not null))
            {
                return new ViewFotoDto();
            }

            //var tienda = await _unitOfWork.TiendaRepository.GetById(createFotoDto.TiendaId);

            var tienda = (await _unitOfWork.TiendaRepository.Find(t => t.Id.Equals(createFotoDto.TiendaId),
                                                                    null,
                                                                    "Fotos",
                                                                    false)).FirstOrDefault();

            if(tienda is not null)
            {
                foto.UrlFoto = await _fileHelper.SaveImage(createFotoDto.UrlFoto, ConstantsApp.Contenedores.ConetenedorDeImagenesTienda);
                foto.IsActve = true;

                if (tienda.Fotos is not null)
                {
                    var fot = tienda.Fotos.ToList();
                    fot.Add(foto);
                    
                    tienda.Fotos = fot;

                    await _unitOfWork.TiendaRepository.Update(tienda);
                }
                else
                {
                    tienda.Fotos = new List<Foto>() { foto };

                    await _unitOfWork.TiendaRepository.Update(tienda);
                }

                
                await _unitOfWork.Save();

                return Mapper.FotoMapper.ToViewFotoDto(foto);
            }
            
            throw new NotImplementedException();
        }

        public Task<ViewFotoDto> Update(CreateFotoDto createFotoDto, int id)
        {
            throw new NotImplementedException();
        }
    }
}
