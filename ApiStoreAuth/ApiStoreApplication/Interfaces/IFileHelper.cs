﻿using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Http;

namespace ApiStore.Application.Interfaces
{
    public interface IFileHelper 
    {

        Task<string> SaveImage(IFormFile image, string? contenedor);
        Task<IFormFile> AdjustSaveImage(IFormFile image, int maxWidth, int maxHeight, string? contenedor);

    }
}
