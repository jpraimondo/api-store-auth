﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Domain.Entities;
using System.Linq.Expressions;

namespace ApiStore.Application.Interfaces
{
    public interface ITiendaService
    {

        Task<IEnumerable<ViewTiendaDto>> GetAll();
        Task<IEnumerable<ViewTiendaDto>> GetAllAndDelete();
        Task<ViewDetailTiendaDto> GetById(int id);
        Task<IEnumerable<ViewTiendaDto>> Find(Expression<Func<Tienda, bool>> predicate);
        public Task<ViewDetailTiendaDto> Insert(CreateTiendaDto tiendaDto);
        public Task<ViewDetailTiendaDto> Update(CreateTiendaDto tiendaDto, int id);
        Task<bool> Delete(int id);
        Task<IEnumerable<ViewTiendaDto>> BuscarPorLocalidad(int id);
    }
}
