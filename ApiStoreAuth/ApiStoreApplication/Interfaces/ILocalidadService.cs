﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Domain.Entities;
using System.Linq.Expressions;

namespace ApiStore.Application.Interfaces
{
    public interface ILocalidadService
    {
        Task<IEnumerable<ViewLocalidadDto>> GetAll();
        Task<IEnumerable<ViewLocalidadDto>> GetAllAndDelete();
        Task<Localidad> GetById(int id);
        IEnumerable<Localidad> Find(Expression<Func<Localidad, bool>> predicate);
        public Task<ViewDetailLocalidadDto> Insert(CreateLocalidadDto localidadDto);
        public Task<Localidad> Update(CreateLocalidadDto localidadDto, int id);
        Task<bool> Delete(int id);

    }
}
