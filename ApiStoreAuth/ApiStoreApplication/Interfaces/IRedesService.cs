﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Domain.Entities;
using System.Linq.Expressions;

namespace ApiStore.Application.Interfaces
{
    public interface IRedesService
    {
        Task<IEnumerable<ViewRedesDto>> GetAll();
        Task<IEnumerable<ViewRedesDto>> GetByStore(int id);
        Task<IEnumerable<ViewRedesDto>> GetAllAndDelete();
        Task<Redes> GetById(int id);
        IEnumerable<Redes> Find(Expression<Func<Redes, bool>> predicate);
        public Task<ViewDetailRedesDto> Insert(CreateRedesDto redDto);
        public Task<ViewDetailRedesDto> Update(CreateRedesDto redDto, int id);
        Task<bool> Delete(int id);
    }
}
