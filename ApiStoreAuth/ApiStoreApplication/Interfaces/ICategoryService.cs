﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Domain.Entities;
using System.Linq.Expressions;

namespace ApiStore.Application.Interfaces
{
    public interface ICategoryService
    {

        Task<IEnumerable<ViewCategoryDto>> GetAll();
        Task<IEnumerable<ViewCategoryDto>> GetAllAndDelete();
        Task<ViewDetailCategoryDto> GetById(int id);
        IEnumerable<Category> Find(Expression<Func<Category, bool>> predicate);
        public Task<ViewDetailCategoryDto> Insert(CreateCategoryDto categoryDto);
        public Task<Category> Update(CreateCategoryDto categoryDto, int id);
        Task<bool> Delete(int id);
    }
}
