﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiStore.Application.Interfaces
{
    public interface IEmailHelper
    {

        public Task<bool> SendEmail(string mailTo);
    }
}
