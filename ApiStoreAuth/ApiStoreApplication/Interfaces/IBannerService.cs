﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Domain.Entities;
using System.Linq.Expressions;

namespace ApiStore.Application.Interfaces
{
    public interface IBannerService
    {

        Task<IEnumerable<ViewDetailBannerDto>> GetAll();
        Task<IEnumerable<ViewDetailBannerDto>> GetAllAndDelete();
        Task<ViewDetailBannerDto> GetById(int? id);
        IEnumerable<ViewDetailBannerDto> Find(Expression<Func<Banner, bool>> predicate);
        public Task<ViewDetailBannerDto> Insert(CreateBannerDto createBannerDto);
        public Task<ViewDetailBannerDto> Update(CreateBannerDto createBannerDto, int id);
        Task<bool> Delete(int id);

    }
}
