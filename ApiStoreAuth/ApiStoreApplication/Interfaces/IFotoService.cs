﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Domain.Entities;
using System.Linq.Expressions;

namespace ApiStore.Application.Interfaces
{
    public interface IFotoService
    {

        Task<IEnumerable<ViewFotoDto>> GetAll();
        Task<IEnumerable<ViewFotoDto>> GetAllAndDelete();
        Task<ViewFotoDto> GetById(int? id);
        IEnumerable<ViewFotoDto> Find(Expression<Func<Foto, bool>> predicate);
        public Task<ViewFotoDto> Insert(CreateFotoDto createFotoDto);
        public Task<ViewFotoDto> Update(CreateFotoDto createFotoDto, int id);
        Task<bool> Delete(int id);
    }
}
