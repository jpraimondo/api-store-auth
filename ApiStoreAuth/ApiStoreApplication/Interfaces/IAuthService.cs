﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.Identity.Models;

namespace ApiStore.Application.Interfaces
{
    public interface IAuthService
    {
       
        Task<ViewAuthDto> Login(CreateAuthDto request);

        Task<ViewAuthDto> Register(RegisterAuthDto request);

        Task<ViewAuthDto> ChangePassword(RegisterAuthDto request);

        
    }
}
