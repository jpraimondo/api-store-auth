﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Domain.Entities;
using System.Linq.Expressions;

namespace ApiStore.Application.Interfaces
{
    public interface IDepartamentoService
    {

        Task<IEnumerable<ViewDepartamentoDto>> GetAll();
        Task<IEnumerable<ViewDepartamentoDto>> GetAllAndDelete();
        Task<ViewDetailDepartamentoDto> GetById(int id);
        IEnumerable<Departamento> Find(Expression<Func<Departamento, bool>> predicate);
        public Task<ViewDetailDepartamentoDto> Insert(CreateDepartamentoDto departamentoDto);
        public Task<ViewDetailDepartamentoDto> Update(CreateDepartamentoDto departamentoDto, int id);
        Task<bool> Delete(int id);
    }
}
