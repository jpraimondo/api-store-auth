﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Domain.Entities;
using System.Linq.Expressions;


namespace ApiStore.Application.Interfaces
{
    public interface INewsletterService
    {

        Task<IEnumerable<ViewNewsletterDto>> GetAll();
        Task<IEnumerable<ViewNewsletterDto>> GetAllAndDelete();
        Task<Newsletter> GetById(int id);
        IEnumerable<ViewNewsletterDto> Find(Expression<Func<Newsletter, bool>> predicate);
        public Task<ViewNewsletterDto> Insert(CreateNewsletterDto newsletterDto);
        public Task<ViewNewsletterDto> Update(CreateNewsletterDto newsletterDto, int id);
        Task<bool> Delete(int id);
    }
}
