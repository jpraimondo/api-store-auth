﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Domain.Entities;

namespace ApiStore.Application.Mapper
{
    public class TiendaMapper
    {
        public static Tienda ToTienda(CreateTiendaDto createTienda)
        {
            return new Tienda()
            {
                Nombre = createTienda.Nombre,
                Descripcion = createTienda.Descripcion,
                LocalidadId = createTienda.LocalidadId,
                Telefono = createTienda.Telefono,
            };
        }


        public static ViewTiendaDto ToViewTiendaDto(Tienda tienda)
        {
            return new ViewTiendaDto()
            {
                Id = tienda.Id,
                Nombre = tienda.Nombre,
                Descripcion = tienda.Descripcion,
                Slug = tienda.Slug,
                Telefono = tienda.Telefono,
                
            };
        }

        public static ViewDetailTiendaDto ToViewDetailDto(Tienda tienda)
        {
            var tiendaDto = new ViewDetailTiendaDto
            {
                Id = tienda.Id,
                Nombre = tienda.Nombre,
                Descripcion = tienda.Descripcion,
                Slug = tienda.Slug,
                Telefono = tienda.Telefono    

            };

            if (tienda.Localidad is not null)
                tiendaDto.Localidad = tienda.Localidad.Nombre;

            if(tienda.Fotos is not null)
            {
                var fotos = new List<ViewDetailFotoDto>();
                
                foreach(Foto f in tienda.Fotos)
                {
                    fotos.Add(new ViewDetailFotoDto
                    {
                       Id = f.Id,
                       UrlFoto = f.UrlFoto,
                       Orden=f.Orden
                       
                    });
                }

                tiendaDto.Fotos = fotos;
            }

            if (tienda.RedesEmpresa is not null)
            {
                var redes = new List<ViewDetailRedesDto>();

                foreach(Redes r in tienda.RedesEmpresa)
                {
                    redes.Add(new ViewDetailRedesDto
                    {
                        Id = r.Id,
                        Nombre = r.Nombre,
                        Link = r.Link,
                    });  
                }
                tiendaDto.RedesEmpresa = redes;
            }

                return tiendaDto;

        }

    }
}
