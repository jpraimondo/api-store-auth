﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Domain.Entities;

namespace ApiStore.Application.Mapper
{
    public class RedesMapper
    {

        public static Redes ToRedes(CreateRedesDto createRedes)
        {
            return new Redes()
            {

                Nombre = createRedes.Nombre,
                Link = createRedes.Link,
                TiendaId = createRedes.TiendaId,
            };
        }

        public static ViewRedesDto ToViewRedesDto(Redes red)
        {
            return new ViewRedesDto()
            {
                Id = red.Id,
                Nombre = red.Nombre,
                Link = red.Link,
            };
        }

        public static ViewDetailRedesDto ToViewDetailRedesDto(Redes red)
        {
            return new ViewDetailRedesDto()
            {
                Id = red.Id,
                Nombre = red.Nombre,
                Link = red.Link,
                TiendaId = red.TiendaId,
            };
        }
    }
}
