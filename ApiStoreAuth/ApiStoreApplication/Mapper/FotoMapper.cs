﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Domain.Entities;
using System.Globalization;

namespace ApiStore.Application.Mapper
{
    public class FotoMapper
    {

        public static Foto ToFoto(CreateFotoDto createFoto)
        {
            return new Foto()
            {
                UrlFoto = string.Empty,
                Orden = createFoto.Orden,
            };
        }

        public static ViewFotoDto ToViewFotoDto(Foto foto)
        {
            return new ViewFotoDto()
            {
                UrlFoto = foto.UrlFoto,
                Orden = foto.Orden,
            };
        }
    }
}
