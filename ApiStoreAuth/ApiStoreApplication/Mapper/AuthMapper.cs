﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.Identity.Models;

namespace ApiStore.Application.Mapper
{
    public class AuthMapper
    {

        public static AuthRequest ToAuthRequest(CreateAuthDto authDto)
        {
            return new AuthRequest
            {
                Email = authDto.Email,
                Password = authDto.Password
            };
        }

        public static RegistrationRequest ToRegisterRequest(RegisterAuthDto authDto)
        {
            return new RegistrationRequest
            {
                Nombre = authDto.Nombre,
                Apellido = authDto.Apellido,
                Username = authDto.Username,
                Email = authDto.Email,
                Password = authDto.Password
            };
        }

    }
}
