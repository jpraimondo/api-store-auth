﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Domain.Entities;

namespace ApiStore.Application.Mapper
{
    public class BannerMapper
    {

        public static Banner ToBanner(CreateBannerDto createBannerDto)
        {
            return new Banner()
            {
                Alternativo = createBannerDto.Alterantivo,
                Orden = createBannerDto.Orden,
                UrlBanner = string.Empty,
            };
        }


        public static ViewDetailBannerDto ToViewDetailBannerDto( Banner banner)
        {
            return new ViewDetailBannerDto()
            {
                Id = banner.Id,
                Alternativo = banner.Alternativo,
                Orden = banner.Orden,
                UrlBanner = banner.UrlBanner,
            };
        }



    }
}
