﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Domain.Entities;

namespace ApiStore.Application.Mapper
{
    public class NewsletterMapper
    {

        public static Newsletter ToNewsletter(CreateNewsletterDto createNewsletter)
        {
            return new Newsletter()
            {
                Correo = createNewsletter.Correo,
            };
        }

        public static ViewNewsletterDto ToViewNewsletterDto(Newsletter newsletter)
        {
            return new ViewNewsletterDto()
            {
                Id = newsletter.Id,
                Correo = newsletter.Correo,
            };
        }
    }
}
