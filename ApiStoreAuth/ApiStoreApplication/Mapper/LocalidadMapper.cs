﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Domain.Entities;

namespace ApiStore.Application.Mapper
{
    public class LocalidadMapper
    {

        public static Localidad ToLocalidad(CreateLocalidadDto createLocalidad)
        {
            return new Localidad()
            {
                Nombre = createLocalidad.Nombre,
                DepartamentoId = createLocalidad.DepartamentoId,

            };
        }

        public static ViewLocalidadDto ToViewLocalidadDto(Localidad localidad)
        {
            return new ViewLocalidadDto()
            {
                Id = localidad.Id,
                Nombre = localidad.Nombre,
            };
        }

        public static ViewDetailLocalidadDto ToViewDetailLocalidadDto(Localidad localidad)
        {
            return new ViewDetailLocalidadDto()
            {
                Id = localidad.Id,
                Nombre = localidad.Nombre,
            };
        }

        public static List<ViewDetailLocalidadDto> ToViewDetailLocalidadDto(List<Localidad> localidades)
        {

            List<ViewDetailLocalidadDto> localidadesDto = new List<ViewDetailLocalidadDto>();

            foreach(var localidad in localidades)
            {
                localidadesDto.Add(new ViewDetailLocalidadDto
                {
                    Id = localidad.Id,
                    Nombre = localidad.Nombre,
                });
            }

            return localidadesDto;
        }
    }
}
