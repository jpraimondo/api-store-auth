﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Domain.Entities;

namespace ApiStore.Application.Mapper
{
    public class DepartamentoMapper
    {


        public static Departamento ToDepartamento(CreateDepartamentoDto createDepartamento)
        {
            return new Departamento()
            {
                Nombre = createDepartamento.Nombre,

            };
        }


        public static ViewDepartamentoDto ToViewDepartamentoDto(Departamento departamento)
        {
            return new ViewDepartamentoDto()
            {
                Id = departamento.Id,
                Nombre = departamento.Nombre,
            };
        }

        public static ViewDetailDepartamentoDto ToViewDetailDepartamentoDto(Departamento departamento)
        {
            return new ViewDetailDepartamentoDto()
            {
                Id = departamento.Id,
                Nombre = departamento.Nombre,
            };
        }

    }
}
