﻿using ApiStore.Application.DTOs.Create;
using ApiStore.Application.DTOs.View;
using ApiStore.Application.DTOs.ViewDetail;
using ApiStore.Domain.Entities;

namespace ApiStore.Application.Mapper
{
    public class CategoryMapper
    {

        public static Category ToCategory(CreateCategoryDto createCategory)
        {
            return new Category()
            {
                Nombre = createCategory.Nombre,

            };
        }

        public static ViewCategoryDto ToViewCategoryDto(Category category)
        {
            return new ViewCategoryDto()
            {
                Id = category.Id,
                Nombre = category.Nombre,
                UrlImagen = category.UrlImagen,
            };
        }

        public static ViewDetailCategoryDto ToViewDetailCategoryDto(Category category)
        {
            return new ViewDetailCategoryDto()
            {
                Id = category.Id,
                Nombre = category.Nombre,
                UrlImagen = category.UrlImagen,

            };
        }


    }
}
