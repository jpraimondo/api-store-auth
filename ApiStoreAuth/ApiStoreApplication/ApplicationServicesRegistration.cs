﻿using ApiStore.Application.Helpers;
using ApiStore.Application.Interfaces;
using ApiStore.Application.Service;
using BazzarWebPage.Application.Helpers;
using Microsoft.Extensions.DependencyInjection;

namespace ApiStore.Application
{
    public static class ApplicationServicesRegistration
    {

        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddTransient<IFileHelper, FileHelper>();
            services.AddTransient<IEmailHelper, EmailHelper>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IDepartamentoService, DepartamentoService>();
            services.AddScoped<ILocalidadService, LocalidadService>();
            services.AddScoped<INewsletterService, NewsletterService>();
            services.AddScoped<IRedesService, RedesService>();
            services.AddScoped<IFotoService, FotoService>();
            services.AddScoped<ITiendaService, TiendaService>();
            services.AddScoped<IBannerService, BannerService>();
            services.AddScoped<IAuthService, AuthService>();



            return services;
        }
    }
}
