﻿using ApiStore.Application.Interfaces;
using ApiStore.Infraestructure.Email.Interface;
using ApiStore.Infraestructure.Email.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiStore.Application.Helpers
{
    public class EmailHelper : IEmailHelper
    {
        private readonly IEmailService _emailService;


        public EmailHelper(IEmailService emailService)
        {
            _emailService = emailService;
        }

        public async Task<bool> SendEmail(string mailTo)
        {
            var builder = new StringBuilder()
                .AppendLine("Muchas gracias por registrarte")
                .AppendLine($"con el correo {mailTo}")
                .AppendLine($"muy pronto recibiras nuestras promociones.")
                .AppendLine($"------------------------------------------")
                .AppendLine("Gracias por confiar en nostros.");

            Email email = new Email()
            {
                To = mailTo,
                Subject = "Te registraste a nuestra web con exito.",
                Body = builder.ToString()
            };

            var result = await _emailService.SendEmail(email);

            return result;
        }
    }
}
