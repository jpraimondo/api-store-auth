﻿using ApiStore.Application.Interfaces;
using ApiStore.Infraestructure.File.Interface;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Http;

namespace BazzarWebPage.Application.Helpers
{
    public class FileHelper : IFileHelper
    {

        private readonly IFileService _fileService;

        public FileHelper(IFileService fileService)
        {
            _fileService = fileService;
        }
                
       
        public Task<IFormFile> AdjustSaveImage(IFormFile image, int maxWidth, int maxHeight, string? contenedor)
        {
            throw new NotImplementedException();
        }

       
        public async Task<string> SaveImage(IFormFile image, string contenedor)
        {
            if(image is not null)
            {
                return await _fileService.CargarArchivo(image,contenedor);
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
