﻿namespace ApiStore.Application.DTOs.ViewDetail
{
    public class ViewDetailBannerDto
    {
        public int Id { get; set; }

        public string UrlBanner { get; set; } = string.Empty;

        public string Alternativo { get; set; } = string.Empty;

        public int Orden { get; set; }
    }
}
