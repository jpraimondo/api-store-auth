﻿
namespace ApiStore.Application.DTOs.ViewDetail
{
    public class ViewDetailDepartamentoDto
    {
        public int Id { get; set; }

        public string? Nombre { get; set; }

        public IEnumerable<ViewDetailLocalidadDto>? Localidades { get; set; }

    }
}
