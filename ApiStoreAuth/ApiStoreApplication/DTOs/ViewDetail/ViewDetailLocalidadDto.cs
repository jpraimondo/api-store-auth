﻿namespace ApiStore.Application.DTOs.ViewDetail
{
    public class ViewDetailLocalidadDto
    {

        public int Id { get; set; }

        public string? Nombre { get; set; }

        public IEnumerable<ViewDetailTiendaDto>? Tiendas { get; set; }

    }
}
