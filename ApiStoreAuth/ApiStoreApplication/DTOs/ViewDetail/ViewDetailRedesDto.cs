﻿namespace ApiStore.Application.DTOs.ViewDetail
{
    public class ViewDetailRedesDto
    {

        public int Id { get; set; }

        public string? Nombre { get; set; }

        public string? Link { get; set; }

        public int TiendaId { get; set; }
    }
}
