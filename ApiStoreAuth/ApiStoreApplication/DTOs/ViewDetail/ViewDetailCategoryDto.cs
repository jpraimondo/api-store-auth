﻿namespace ApiStore.Application.DTOs.ViewDetail
{
    public class ViewDetailCategoryDto
    {
        public int Id { get; set; }

        public string? Nombre { get; set; }

        public string? UrlImagen { get; set; }

        public IEnumerable<ViewDetailTiendaDto>? Tiendas { get; set; }
       
    }
}
