﻿namespace ApiStore.Application.DTOs.ViewDetail
{
    public class ViewDetailFotoDto
    {

        public int Id { get; set; }

        public string? UrlFoto { get; set; }

        public int Orden { get; set; }


    }
}
