﻿namespace ApiStore.Application.DTOs.ViewDetail
{
    public class ViewDetailNewsletterDto
    {
        public int Id { get; set; }

        public string? Correo { get; set; }
    }
}
