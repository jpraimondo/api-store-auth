﻿namespace ApiStore.Application.DTOs.ViewDetail
{
    public class ViewDetailTiendaDto
    {
        public int Id { get; set; }

        public string Nombre { get; set; } = string.Empty;

        public string Descripcion { get; set; } = string.Empty;

        public string Slug { get; set; } = string.Empty;

        public string Telefono { get; set; } = string.Empty;

        public string Localidad { get; set; } = string.Empty;

        public IEnumerable<ViewDetailFotoDto>? Fotos { get; set; }

        public IEnumerable<ViewDetailRedesDto>? RedesEmpresa { get; set; }
    }
}
