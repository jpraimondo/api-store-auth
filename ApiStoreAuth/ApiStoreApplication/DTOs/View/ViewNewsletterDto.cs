﻿namespace ApiStore.Application.DTOs.View
{
    public class ViewNewsletterDto
    {
        public int Id { get; set; }

        public string? Correo { get; set; }
    }
}
