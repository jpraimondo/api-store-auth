﻿namespace ApiStore.Application.DTOs.View
{
    public class ViewTiendaDto
    {
        public int Id { get; set; }

        public string Nombre { get; set; } = string.Empty;

        public string Descripcion { get; set; } = string.Empty;

        public string Slug { get; set; } = string.Empty;

        public string Telefono { get; set; } = string.Empty;

        public string LocalidadNombre { get; set; } = string.Empty;

        public IEnumerable<ViewFotoDto>? Fotos { get; set; }

        public IEnumerable<ViewRedesDto>? RedesEmpresa { get; set; }
    }
}
