﻿using ApiStore.Application.Identity.Models;

namespace ApiStore.Application.DTOs.View
{
    public class ViewAuthDto
    {
        public string Message { get; set; } = string.Empty;

        public AuthResponse? AuthResponse { get; set; }
    }
}
