﻿namespace ApiStore.Application.DTOs.View
{
    public class ViewFotoDto
    {

        public int Id { get; set; }

        public string? UrlFoto { get; set; }

        public int Orden { get; set; }


    }
}
