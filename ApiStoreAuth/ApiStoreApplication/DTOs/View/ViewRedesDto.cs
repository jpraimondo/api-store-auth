﻿namespace ApiStore.Application.DTOs.View
{
    public class ViewRedesDto
    {

        public int Id { get; set; }

        public string? Nombre { get; set; }

        public string? Link { get; set; }
    }
}
