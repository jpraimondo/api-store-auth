﻿namespace ApiStore.Application.DTOs.View;

public class ViewLocalidadDto
{

    public int Id { get; set; }

    public string? Nombre { get; set; }

}
