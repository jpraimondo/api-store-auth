﻿namespace ApiStore.Application.DTOs.Create
{
    public class CreateAuthDto
    {
        public string Email { get; set; } = string.Empty;

        public string Password { get; set; } = string.Empty;
    }
}
