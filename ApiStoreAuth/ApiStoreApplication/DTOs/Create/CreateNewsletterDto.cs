﻿using System.ComponentModel.DataAnnotations;

namespace ApiStore.Application.DTOs.Create
{
    public class CreateNewsletterDto
    {

        [Required]
        [DataType(DataType.EmailAddress)]
        public string? Correo { get; set; }

    }
}
