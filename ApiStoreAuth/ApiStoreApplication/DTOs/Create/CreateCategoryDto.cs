﻿using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace ApiStore.Application.DTOs.Create
{
    public class CreateCategoryDto
    {

        [Required]
        public string? Nombre { get; set; }

        public IFormFile? UrlImagen { get; set; }


    }
}
