﻿using System.ComponentModel.DataAnnotations;

namespace ApiStore.Application.DTOs.Create
{
    public class CreateTiendaDto
    {
        [Required]
        public string? Nombre { get; set; }

        [Required]
        public string? Descripcion { get; set; }

        [Required]
        public int LocalidadId { get; set; }

        [Required]
        public int CategoriaId { get; set; }

        [Required]
        public string? Telefono { get; set; }
    }
}
