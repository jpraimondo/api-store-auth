﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace ApiStore.Application.DTOs.Create
{
    public class CreateFotoDto
    {

        [Required]
        public IFormFile? UrlFoto { get; set; }

        [Required]
        public int Orden { get; set; }

        [Required]
        public int TiendaId { get; set; }

    }
}
