﻿using System.ComponentModel.DataAnnotations;

namespace ApiStore.Application.DTOs.Create;

public class CreateLocalidadDto
{

    [Required]
    public string? Nombre { get; set; }

    [Required]
    public int DepartamentoId { get; set; }


}
