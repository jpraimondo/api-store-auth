﻿using System.ComponentModel.DataAnnotations;

namespace ApiStore.Application.DTOs.Create
{
    public class CreateRedesDto
    {

        [Required]
        public string? Nombre { get; set; }
        [Required]
        public string? Link { get; set; }
        [Required]
        public int TiendaId { get; set; }
    }
}
