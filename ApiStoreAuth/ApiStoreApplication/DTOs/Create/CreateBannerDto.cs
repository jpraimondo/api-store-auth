﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace ApiStore.Application.DTOs.Create
{
    public class CreateBannerDto
    {
        [Required]
        public IFormFile? BannerImage { get; set; }

        [Required]
        public string Alterantivo { get; set; } = string.Empty;

        public int Orden { get; set; }=1;

    }
}
