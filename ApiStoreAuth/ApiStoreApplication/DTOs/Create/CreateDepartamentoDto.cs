﻿using System.ComponentModel.DataAnnotations;

namespace ApiStore.Application.DTOs.Create
{
    public class CreateDepartamentoDto
    {
        [Required]
        public string? Nombre { get; set; }
    }
}
