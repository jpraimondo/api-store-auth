﻿namespace ApiStore.Domain.Enums
{
    public enum NombreRed
    {
        WEBPAGE,
        TWITTER,
        FACEBOOK,
        INSTAGRAM,
        TIKTOK,
        LINKEDIN
    }
}
