﻿namespace ApiStore.Domain.Constants
{
    public class ConstantsApp
    {

        public static class Contenedores
        {
            public const string ConetenedorDeDocumentos = "docs";

            public const string ConetenedorDeImagenes = "public";

            public const string ConetenedorDeImagenesCategoria = "category";

            public const string ConetenedorDeImagenesTienda = "store";
        }

    }
}
