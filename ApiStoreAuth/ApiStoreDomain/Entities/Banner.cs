﻿using System.ComponentModel.DataAnnotations;

namespace ApiStore.Domain.Entities
{
    public class Banner:EntityBase
    {

        [Required]
        [StringLength(255)]
        public string UrlBanner { get; set; } = string.Empty;

        [Required]
        [StringLength(255)]
        public string Alternativo { get; set; } = string.Empty;

        [Required]
        public int Orden { get; set; } = 1;



    }
}
