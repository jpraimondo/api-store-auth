﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ApiStore.Domain.Entities
{
    public class Localidad:EntityBase
    {

        [Required]
        [StringLength(50)]
        public string Nombre { get; set; } = string.Empty;

        [Required]
        [ForeignKey("Departamento")]
        public int DepartamentoId { get; set; }
        public virtual Departamento? Departamento { get; set; }


        public IList<Tienda>? Tiendas { get; set; }
    }
}
