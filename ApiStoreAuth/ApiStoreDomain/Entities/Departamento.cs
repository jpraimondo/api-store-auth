﻿using System.ComponentModel.DataAnnotations;

namespace ApiStore.Domain.Entities
{
    public class Departamento:EntityBase
    {
        [StringLength(50)]
        public string Nombre { get; set; }=string.Empty;

        public virtual IEnumerable<Localidad>? Localidades { get; set; }

    }
}
