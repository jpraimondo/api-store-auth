﻿using System.ComponentModel.DataAnnotations;

namespace ApiStore.Domain.Entities
{
    public class Newsletter : EntityBase
    {

        [Required]
        [DataType(DataType.EmailAddress)]
        [StringLength(100)]
        public string? Correo { get; set; }=string.Empty;


    }
}
