﻿using System.ComponentModel.DataAnnotations;

namespace ApiStore.Domain.Entities
{
    public class Foto : EntityBase
    {
        [Required]
        [StringLength(255)]
        public string UrlFoto { get; set; } =string.Empty;

        [Required]
        public int Orden { get; set; }

    }
}
