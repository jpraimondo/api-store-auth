﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ApiStore.Domain.Entities
{
    public class Tienda:EntityBase
    {
        [Required]
        [StringLength(50)]
        public string Nombre { get; set; } = string.Empty;

        [StringLength(255)]
        public string Descripcion { get; set; } = string.Empty;

        [Required]
        [StringLength(255)]
        public string Slug { get; set; } = string.Empty;

        [Required]
        [ForeignKey("Localidad")]
        public int LocalidadId { get; set; }
        public virtual Localidad? Localidad { get; set; }

        [Required]
        [ForeignKey("Category")]
        public int CategoryId { get; set; }
        public virtual Category? Category { get; set; }


        public virtual IEnumerable<Foto>? Fotos { get; set; }

        public bool Estado { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string Telefono { get; set; }=string.Empty;

        public virtual IEnumerable<Redes>? RedesEmpresa { get; set; }
    }
}
