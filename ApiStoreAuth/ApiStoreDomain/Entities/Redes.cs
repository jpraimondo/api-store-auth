﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ApiStore.Domain.Entities
{
    public class Redes : EntityBase
    {

        [Required]
        [StringLength(50)]
        public string Nombre { get; set; } = string.Empty;
        [Required]
        [StringLength(255)]
        public string Link { get; set; } = string.Empty;

        [ForeignKey("Tienda")]
        public int TiendaId { get; set; }
        public virtual Tienda? Tienda { get; set; }
    }
}
