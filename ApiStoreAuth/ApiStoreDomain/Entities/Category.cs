﻿using System.ComponentModel.DataAnnotations;

namespace ApiStore.Domain.Entities
{
    public class Category :EntityBase
    {

        [Required]
        [StringLength(50)]
        public string Nombre { get; set; } = string.Empty;

        [StringLength(255)]
        public string UrlImagen { get; set; } = string.Empty;

        public int CategoryPadre { get; set; }

        public IList<Tienda>? Tiendas{ get; set; } 


    }
}
