﻿using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using ApiStore.Infraestructure.File.Interface;
using Azure.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using static System.Formats.Asn1.AsnWriter;

namespace ApiStore.Infraestructure.File
{
    internal class AwsFileService : IFileService
    {
        private readonly IAwsConfiguration awsConfiguration;
        private readonly IAmazonS3 amazonS3;

        public AwsFileService(IAmazonS3 amazonS3, IAwsConfiguration awsConfiguration)
        {
            this.amazonS3 = amazonS3;
            this.awsConfiguration = awsConfiguration;
        }

        public async Task<string> CargarArchivo(IFormFile file, string container)
        {


            try
            {
                var extension = Path.GetExtension(file.FileName);

                var credentials = new BasicAWSCredentials(awsConfiguration.AwsAccessKey, 
                                                          awsConfiguration.AwsSecretAccessKey);

                var config = new AmazonS3Config()
                {
                    RegionEndpoint = Amazon.RegionEndpoint.USEast1,
                    USEast1RegionalEndpointValue = S3UsEast1RegionalEndpointValue.Regional,
                    
                };

                using var client = new AmazonS3Client(credentials, config);

                var uploadRequest = new TransferUtilityUploadRequest()
                {
                    BucketName = "aws-api-store-bucket",
                    Key = $"{Guid.NewGuid()}{extension}",
                    FilePath = container,
                    InputStream = file.OpenReadStream(),
                    ContentType = file.ContentType,
                    CannedACL = S3CannedACL.NoACL
                };


                var transferUtility = new TransferUtility(client);

                await transferUtility.UploadAsync(uploadRequest);

                //var requestObject = new PutObjectRequest()
                //{
                //    BucketName = "aws-api-store-bucket",
                //    Key = $"{Guid.NewGuid()}{extension}",
                //    FilePath = container,
                //    InputStream = file.OpenReadStream(),
                //    ContentType = file.ContentType,
                //    CannedACL = S3CannedACL.NoACL
                //};

                //PutObjectResponse response = await amazonS3.PutObjectAsync(requestObject);


                return $"https://{uploadRequest.BucketName}.s3.amazonaws.com/{container}/{uploadRequest.Key}";

                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }




            throw new NotImplementedException();
        }
    }
}
