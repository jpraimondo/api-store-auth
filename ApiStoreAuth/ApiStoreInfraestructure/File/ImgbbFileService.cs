﻿using ApiStore.Infraestructure.File.Interface;
using EllipticCurve.Utils;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using RestSharp;


namespace ApiStore.Infraestructure.File
{
    internal class ImgbbFileService : IFileService
    {

        private readonly string key;

        public ImgbbFileService()
        {
            key = "4c9b85f146ce1be9eec88def9d2f1e60";
        }

        public async Task<string> CargarArchivo(IFormFile file, string container)
        {
            try
            {

            using (var client = new RestClient("https://api.imgbb.com/1/upload"))
            {
                var extension = Path.GetExtension(file.FileName);

                var name = $"{Guid.NewGuid()}";


                var request = new RestRequest("", Method.Post);

               
                request.AddParameter("key", key);
                request.AddParameter("name", name);
               

                await using var imagen = new MemoryStream();
                await file.CopyToAsync(imagen);
                
                request.AddFile("image", imagen.ToArray(),name, file.ContentType);
                    

                var result = await client.ExecuteAsync(request);

                if(result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dynamic jsonObj = JsonConvert.DeserializeObject(result.Content);
                    return jsonObj["data"]["display_url"].ToString();
                }
               
                return string.Empty;
            }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

    }
}
