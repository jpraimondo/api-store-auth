﻿using ApiStore.Infraestructure.File.Interface;
using Azure.Core;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Microsoft.AspNetCore.Http;

namespace ApiStore.Infraestructure.File
{
    public class AzureFileService:IFileService
    {

        public readonly BlobServiceClient _blobClient;


        public AzureFileService(BlobServiceClient blobClient)
        {
            _blobClient = blobClient;
        }


           public async Task<string> CargarArchivo(IFormFile file, string container)
        {
            
            try
            {
                BlobContainerClient contenedor = _blobClient.GetBlobContainerClient(container);
                
                await contenedor.CreateIfNotExistsAsync();

                await contenedor.SetAccessPolicyAsync(Azure.Storage.Blobs.Models.PublicAccessType.Blob);

                var extension = Path.GetExtension(file.FileName);

                var fileName = $"{Guid.NewGuid()}{extension}";
                
                var blob = contenedor.GetBlobClient(fileName);

                await blob.UploadAsync(file.OpenReadStream(), new BlobHttpHeaders { ContentType = file.ContentType });

                return blob.Uri.ToString();

            }
            catch(Exception ex)
            {
                throw new Exception($"Error al cargar el archivo. {ex.Message}");
            }



            
        } 


    }
}
