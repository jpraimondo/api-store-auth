﻿using Microsoft.AspNetCore.Http;

namespace ApiStore.Infraestructure.File.Interface
{
    public interface IFileService
    {

        Task<string> CargarArchivo(IFormFile file, string container);
    }
}
