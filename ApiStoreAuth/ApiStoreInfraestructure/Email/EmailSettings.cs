﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiStore.Infraestructure.Email
{
    public class EmailSettings
    {
        public string ApiKey { get; set; } = ""; //"***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***";
        public string FromAddress { get; set; } = "contacto@quammit.com";
        public string FromName { get; set; } = "QuammIT Contact";
    }
}
