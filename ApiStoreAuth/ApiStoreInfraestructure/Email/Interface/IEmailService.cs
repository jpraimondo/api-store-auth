﻿namespace ApiStore.Infraestructure.Email.Interface
{
    public interface IEmailService
    {
        Task<bool> SendEmail(Models.Email email);
    }
}
