﻿using Amazon.S3;
using ApiStore.Infraestructure.Email.Interface;
using ApiStore.Infraestructure.Email;
using ApiStore.Infraestructure.File;
using ApiStore.Infraestructure.File.Interface;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ApiStore.Infraestructure
{
    public static class InfraestructureServiceRegistration
    {
        public static IServiceCollection AddConfigureInfraestructureServices(this IServiceCollection services, IConfiguration configuration)
        {

            //services.AddSingleton(new BlobServiceClient(configuration.GetConnectionString("BlobStorage")));
            services.AddSingleton<IAwsConfiguration, AwsConfiguration>();
            services.AddAWSService<IAmazonS3>();
            services.AddSingleton<IFileService, ImgbbFileService>();

            services.Configure<EmailSettings>(c => configuration.GetSection("EmailSettings").GetChildren().ToList());
            services.AddTransient<IEmailService, EmailService>();

            return services;
        }

    }
}
