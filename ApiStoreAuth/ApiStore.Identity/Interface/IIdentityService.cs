﻿using ApiStore.Application.Identity.Models;
using ApiStore.Identity.Models;
using System.IdentityModel.Tokens.Jwt;

namespace ApiStore.Application.Identity.Interfaces
{
    public interface IIdentityService
    {
        Task<AuthResponse> Login(AuthRequest request);

        Task<AuthResponse> Register(RegistrationRequest request);

        Task<AuthResponse> ChangePassword(RegistrationRequest request);

        Task<JwtSecurityToken> GenerateToken(ApplicationUser user);

        Task<AuthResponse> SetPassword(RegistrationRequest request);
    }
}
