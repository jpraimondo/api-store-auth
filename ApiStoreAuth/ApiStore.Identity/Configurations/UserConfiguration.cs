﻿using ApiStore.Identity.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CleanArchitecture.Identity.Configurations
{
    internal class UserConfiguration : IEntityTypeConfiguration<ApplicationUser>
    {

        

        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            var hasher = new PasswordHasher<ApplicationUser>();

            var usuario = new ApplicationUser
            {
                Id = "c2711be2-af4c-43a3-8d9b-0791228cf5bd",
                Email = "admin@localhost.com",
                NormalizedEmail = "admin@localhost.com",
                Nombre = "Admin",
                Apellido = "User",
                NormalizedUserName="ADMINISTRATOR",
                UserName = "administrator",
                EmailConfirmed = true,
            };

            usuario.PasswordHash = hasher.HashPassword(usuario, "Password1*");


            builder.HasData(usuario);

        }
    }
}
