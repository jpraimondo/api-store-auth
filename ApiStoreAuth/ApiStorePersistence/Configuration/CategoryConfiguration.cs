﻿using ApiStore.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiStore.Persistence.Configuration
{
    public class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
       

        public void Configure(EntityTypeBuilder<Category> builder)
        {
            var time = DateTime.UtcNow;


            builder.HasData(
                new Category() { Id = 1,Nombre = "Empresas", UrlImagen = "https://i.ibb.co/fd6NNn8/70348734-f69c-4d70-8a3b-8e56cfa239f6.jpg", CategoryPadre = 0, IsDelete = false, IsActve = true, CreateBy = "System", CreateTime = time },
                new Category() { Id = 2, Nombre = "Servicios", UrlImagen = "https://i.ibb.co/fd6NNn8/70348734-f69c-4d70-8a3b-8e56cfa239f6.jpg", CategoryPadre = 0, IsDelete = false, IsActve = true, CreateBy = "System", CreateTime = time },
                new Category() { Id = 3, Nombre = "Transporte", UrlImagen = "https://i.ibb.co/fd6NNn8/70348734-f69c-4d70-8a3b-8e56cfa239f6.jpg", CategoryPadre = 0, IsDelete = false, IsActve = true, CreateBy = "System", CreateTime = time },
                new Category() { Id = 4, Nombre = "Publico", UrlImagen = "https://i.ibb.co/fd6NNn8/70348734-f69c-4d70-8a3b-8e56cfa239f6.jpg", CategoryPadre = 0, IsDelete = false, IsActve = true, CreateBy = "System", CreateTime = time }
                );
        }
    }
}
