﻿using ApiStore.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiStore.Persistence.Configuration
{
    internal class DepartamentoConfiguration : IEntityTypeConfiguration<Departamento>
    {
        public void Configure(EntityTypeBuilder<Departamento> builder)
        {
            var time = DateTime.UtcNow;

            builder.HasData(
               new Departamento() { Id = 1, IsActve = true, Nombre = "Montevideo", IsDelete = false, CreateBy="System", CreateTime=time},
                new Departamento() { Id = 2, IsActve = true, Nombre = "Canelones", IsDelete = false, CreateBy = "System", CreateTime = time },
                new Departamento() { Id = 3, IsActve = true, Nombre = "Maldonado", IsDelete = false, CreateBy = "System", CreateTime = time },
                new Departamento() {Id = 4, IsActve = true, Nombre = "Rocha", IsDelete = false, CreateBy = "System", CreateTime = time },
                new Departamento() {Id = 5, IsActve = true, Nombre = "Colonia", IsDelete = false, CreateBy = "System", CreateTime = time },
                new Departamento() {Id = 6, IsActve = true, Nombre = "San Jose", IsDelete = false, CreateBy = "System", CreateTime = time },
                new Departamento() {Id = 7, IsActve = true, Nombre = "Flores", IsDelete = false, CreateBy = "System", CreateTime = time },
                new Departamento() {Id = 8, IsActve = true, Nombre = "Lavalleja", IsDelete = false, CreateBy = "System", CreateTime = time },
                new Departamento() {Id = 9, IsActve = true, Nombre = "Treinta y tres", IsDelete = false, CreateBy = "System", CreateTime = time },
                new Departamento() {Id = 10, IsActve = true, Nombre = "Cerro Largo", IsDelete = false, CreateBy = "System", CreateTime = time },
                new Departamento() {Id = 11, IsActve = true, Nombre = "Durazno", IsDelete = false, CreateBy = "System", CreateTime = time },
                new Departamento() {Id = 12, IsActve = true, Nombre = "Tacuarembo", IsDelete = false, CreateBy = "System", CreateTime = time },
                new Departamento() {Id = 13, IsActve = true, Nombre = "Rivera", IsDelete = false, CreateBy = "System", CreateTime = time },
                new Departamento() {Id = 14, IsActve = true, Nombre = "Artigas", IsDelete = false, CreateBy = "System", CreateTime = time },
                new Departamento() {Id = 15, IsActve = true, Nombre = "Salto", IsDelete = false, CreateBy = "System", CreateTime = time },
                new Departamento() {Id = 16, IsActve = true, Nombre = "Paysandú", IsDelete = false, CreateBy = "System", CreateTime = time },
                new Departamento() {Id = 17, IsActve = true, Nombre = "Rio Negro", IsDelete = false, CreateBy = "System", CreateTime = time },
                new Departamento() {Id = 18, IsActve = true, Nombre = "Soriano", IsDelete = false, CreateBy = "System", CreateTime = time },
                new Departamento() {Id = 19, IsActve = true, Nombre = "Florida", IsDelete = false, CreateBy = "System", CreateTime = time }
                );
        }
    }
}
