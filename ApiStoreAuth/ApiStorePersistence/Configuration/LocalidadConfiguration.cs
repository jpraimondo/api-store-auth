﻿using ApiStore.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiStore.Persistence.Configuration
{
    internal class LocalidadConfiguration : IEntityTypeConfiguration<Localidad>
    {
        public void Configure(EntityTypeBuilder<Localidad> builder)
        {
            var time = DateTime.UtcNow;

            builder.HasData(
                new Localidad() { Id = 1, IsActve = true, Nombre = "Montevideo", IsDelete = false, DepartamentoId = 1,CreateBy = "System", CreateTime = time },
                new Localidad() { Id = 2, IsActve = true, Nombre = "Atlantida", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() { Id = 3, IsActve = true, Nombre = "Canelones", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() { Id = 4, IsActve = true, Nombre = "Paso Carrasco", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() { Id = 5, IsActve = true, Nombre = "Ciudad de la Costa", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() { Id = 6, IsActve = true, Nombre = "Colonia Nicolich", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() { Id = 7, IsActve = true, Nombre = "Pando", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() {Id = 8, IsActve = true, Nombre = "Salinas", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() {Id = 9, IsActve = true, Nombre = "Parque del Plata", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() {Id = 10, IsActve = true, Nombre = "Soca", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() {Id = 11, IsActve = true, Nombre = "Empalme Olmos", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() {Id = 12, IsActve = true, Nombre = "San Jacinto", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() {Id = 13, IsActve = true, Nombre = "Migues", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() {Id = 14, IsActve = true, Nombre = "Montes", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() {Id = 15, IsActve = true, Nombre = "Tala", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() {Id = 16, IsActve = true, Nombre = "Santa Rosa", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() {Id = 17, IsActve = true, Nombre = "San Antonio", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() {Id = 18, IsActve = true, Nombre = "San Ramon", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() {Id = 19, IsActve = true, Nombre = "San Bautista", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() {Id = 20, IsActve = true, Nombre = "Santa Lucia", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() {Id = 21, IsActve = true, Nombre = "Los Cerrillos", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() {Id = 22, IsActve = true, Nombre = "Progreso", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() {Id = 23, IsActve = true, Nombre = "La Paz", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() {Id = 24, IsActve = true, Nombre = "Las Piedras", IsDelete = false, DepartamentoId = 2, CreateBy = "System", CreateTime = time },
                new Localidad() { Id = 25, IsActve = true, Nombre = "San Jose", IsDelete = false, DepartamentoId = 3, CreateBy = "System", CreateTime = time }
                );

        }
    }
}
