﻿using ApiStore.Domain.Entities;
using ApiStore.Persistence.Persistence;
using ApiStore.Persistence.Repositories.Interfaces;

namespace ApiStore.Persistence.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApiStoreDbContext context;

        private IGenericRepository<Category>? categoryRepository;

        private IGenericRepository<Departamento>? departamentoRepository;

        private IGenericRepository<Foto>? fotoRepository;

        private IGenericRepository<Localidad>? localidadRepository;

        private IGenericRepository<Newsletter>? newsletterRepository;

        private IGenericRepository<Redes>? redesRepository;

        private IGenericRepository<Tienda>? tiendaRepository;

        private IGenericRepository<Banner>? bannerRepository;

        public IGenericRepository<Category> CategoryRepository
        {
            get
            {
                if(categoryRepository == null)
                {
                    categoryRepository = new GenericRepository<Category>(context);
                }
                return categoryRepository;
            }
        }

        public IGenericRepository<Departamento> DepartamentoRepository
        {
            get
            {
                if (departamentoRepository == null)
                {
                    departamentoRepository = new GenericRepository<Departamento>(context);
                }
                return departamentoRepository;
            }
        }

        public IGenericRepository<Foto> FotoRepository
        {
            get
            {
                if(fotoRepository==null)
                {
                    fotoRepository = new GenericRepository<Foto>(context); 
                }
                return fotoRepository;
            }
        }

        public IGenericRepository<Localidad> LocalidadRepository
        {
            get
            {
                if(localidadRepository==null)
                {
                    localidadRepository = new GenericRepository<Localidad>(context);
                }
                return localidadRepository;
            }
        }

        public IGenericRepository<Newsletter> NewsletterRepository
        {
            get
            {
                if(newsletterRepository==null)
                {
                    newsletterRepository = new GenericRepository<Newsletter>(context);
                }
                return newsletterRepository;
            }
        }

        public IGenericRepository<Redes> RedesRepository
        {
            get
            {
                if(redesRepository==null)
                {
                    redesRepository = new GenericRepository<Redes>(context);
                }
                return redesRepository;
            }
        }

        public IGenericRepository<Tienda> TiendaRepository
        {
            get
            {
                if(tiendaRepository==null)
                {
                    tiendaRepository = new GenericRepository<Tienda>(context);
                }
                return tiendaRepository;
            }
        }

        public IGenericRepository<Banner> BannerRepository
        {
            get
            {
                if (bannerRepository == null)
                {
                    bannerRepository = new GenericRepository<Banner>(context);
                }
                return bannerRepository;
            }
        }


        public UnitOfWork(ApiStoreDbContext ctx)
        {
            context = ctx;
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public async Task Save()
        {
            await context.SaveChangesAsync();
        }
    }
}
