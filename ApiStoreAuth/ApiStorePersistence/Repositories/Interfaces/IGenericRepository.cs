﻿using ApiStore.Domain.Entities;
using System.Linq.Expressions;

namespace ApiStore.Persistence.Repositories.Interfaces
{
    public interface IGenericRepository<T> where T : EntityBase
    {
        Task<IEnumerable<T>> GetAll();
        Task<IEnumerable<T>> GetAllAndDelete();
        Task<IEnumerable<T>> Find(Expression<Func<T, bool>> predicate);

        Task<IEnumerable<T>> Find(Expression<Func<T, bool>> predicate = null,
                                      Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
                                      string includeString = null,
                                      bool disableTracking = true);
        Task Activate(T entity);
        Task Inactivate(T entity);
        Task Insert(T entity);
        Task Update(T entity);
        Task Delete(T entity);
        Task<T> GetById(int id);
    }
}
