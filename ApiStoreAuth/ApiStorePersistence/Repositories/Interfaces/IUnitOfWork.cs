﻿using ApiStore.Domain.Entities;

namespace ApiStore.Persistence.Repositories.Interfaces
{
    public interface IUnitOfWork:IDisposable
    {
        public IGenericRepository<Category> CategoryRepository { get; }
        public IGenericRepository<Departamento> DepartamentoRepository { get; }
        public IGenericRepository<Foto> FotoRepository { get; }
        public IGenericRepository<Localidad> LocalidadRepository { get; }
        public IGenericRepository<Newsletter> NewsletterRepository { get; }
        public IGenericRepository<Redes> RedesRepository { get; }
        public IGenericRepository<Tienda> TiendaRepository { get; }
        public IGenericRepository<Banner> BannerRepository { get; }

        Task Save();

    }
}
