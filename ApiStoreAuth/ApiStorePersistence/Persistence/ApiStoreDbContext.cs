﻿using ApiStore.Domain.Entities;
using ApiStore.Persistence.Configuration;
using Microsoft.EntityFrameworkCore;

namespace ApiStore.Persistence.Persistence
{
    public class ApiStoreDbContext : DbContext
    {
        public ApiStoreDbContext(DbContextOptions<ApiStoreDbContext> options)
            : base(options)
        {

        }
        public DbSet<Category>? Categorias { get; set; }
        public DbSet<Departamento>? Departamentos { get; set; }
        public DbSet<Localidad>? Localidades { get; set; }
        public DbSet<Newsletter>? Newsletters { get; set; }
        public DbSet<Foto>? Fotos { get; set; }
        public DbSet<Tienda>? Tiendas { get; set; }
        public DbSet<Redes>? Redes { get; set; }
        public DbSet<Banner>? Banners { get; set; }



        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new DepartamentoConfiguration());
            builder.ApplyConfiguration(new LocalidadConfiguration());
            builder.ApplyConfiguration(new CategoryConfiguration());
        }

    }
}
