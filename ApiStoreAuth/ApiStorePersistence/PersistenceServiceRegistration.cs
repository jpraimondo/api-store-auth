﻿using ApiStore.Persistence.Persistence;
using ApiStore.Persistence.Repositories;
using ApiStore.Persistence.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace ApiStore.Persistence
{
    public static class PersistenceServiceRegistration
    {
        public static IServiceCollection AddPersistanceService(this IServiceCollection services, IConfiguration configuration)
        {
            //services.AddDbContext<ApiStoreDbContext>(options =>
            //options.UseSqlServer(configuration.GetConnectionString("StoreConnection"))
            //);

            var connectionString = configuration.GetConnectionString("WebApiDatabase");

            services.AddDbContext<ApiStoreDbContext>(options =>
            options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString)
            ));


            services.AddScoped<IUnitOfWork, UnitOfWork>();

            return services;
        }
    }
}
